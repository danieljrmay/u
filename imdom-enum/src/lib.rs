pub mod dom;
pub mod namespaces;
pub mod uri;
pub mod xhtml;
pub mod xml;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
