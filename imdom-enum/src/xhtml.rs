use super::dom::Attribute;
use super::dom::AttributeSet;
use super::dom::Element;
use super::dom::Node;
use super::namespaces::Namespace;

pub const NAMESPACE_PREFIX: &str = "xhtml";
pub const NAMESPACE_URI: &str = "http://www.w3.org/1999/xhtml";

pub enum XHTMLAttribute {
    Title(String),
}
impl Attribute for XHTMLAttribute {
    fn namespace(&self) -> Namespace {
        Namespace::XHTML
    }

    fn local_name(&self) -> &str {
        match self {
            XHTMLAttribute::Title(_title) => "title",
        }
    }

    fn value_as_str(&self) -> &str {
        match self {
            XHTMLAttribute::Title(title) => title,
        }
    }
}

pub enum XHTMLElement<'a> {
    P(AttributeSet<'a>, Vec<Node>),
}
impl<'a> Element for XHTMLElement<'a> {
    fn namespace(&self) -> Namespace {
        Namespace::XHTML
    }

    fn local_name(&self) -> &str {
        match self {
            XHTMLElement::P(_attribute_set, _children) => "p",
        }
    }

    fn attributes(&self) -> Option<AttributeSet> {
        None
    }

    fn children(&self) -> Option<Node> {
        None
    }
}
