use imdom_enum::namespaces::Namespace;
use imdom_enum::xml;

fn main() {
    println!("Testing imdom-enum");

    let xhtml_ns = Namespace::XHTML;
    println!(
        "Prefixed namespace{}",
        xml::serialize_prefixed_namespace(&xhtml_ns)
    );
    println!(
        "Default namespace{}",
        xml::serialize_default_namespace(&xhtml_ns)
    );
}
