use super::namespaces::Namespace;

pub trait Attribute {
    fn namespace(&self) -> Namespace;
    fn local_name(&self) -> &str;
    fn value_as_str(&self) -> &str;
}

pub struct AttributeSet<'a> {
    attributes: Vec<&'a Attribute>,
}
impl<'a> AttributeSet<'a> {}

pub trait Element {
    fn namespace(&self) -> Namespace;
    fn local_name(&self) -> &str;
    fn attributes(&self) -> Option<AttributeSet>;
    fn children(&self) -> Option<Node>;
}

pub enum Node {
    CommentNode(String),
    ElementNode(Box<Element>),
    ProcessingInstructionNode(String, String),
    TextNode(String),
}
