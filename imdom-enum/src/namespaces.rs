use super::uri::Uri;
use super::xhtml;
use super::xml;

const NAMESPACE_PREFIX: &str = "xmlns";
const NAMESPACE_URI: &str = "http://www.w3.org/2000/xmlns/";

pub struct Prefix {
    prefix: String,
}
impl Prefix {
    fn new(prefix: String) -> Prefix {
        // Change name to NCName to match namespace specification?
        // TODO validate prefix
        Prefix { prefix }
    }

    fn as_str(&self) -> &str {
        &self.prefix
    }
}

pub enum Namespace {
    XMLNS,
    XML,
    XHTML,
    //    MathML,
    //    SVG,
    //    U,
    Custom(Prefix, Uri),
}
impl Namespace {
    fn new(prefix: Prefix, uri: Uri) -> Namespace {
        // TODO convert signature to &str, &str ? Have Prefix and Uri as private
        Namespace::Custom(prefix, uri)
    }

    pub fn prefix_as_str(&self) -> &str {
        match self {
            Namespace::XMLNS => NAMESPACE_PREFIX,
            Namespace::XML => xml::NAMESPACE_PREFIX,
            Namespace::XHTML => xhtml::NAMESPACE_PREFIX,
            //    MathML,
            //    SVG,
            //    U,
            Namespace::Custom(p, _u) => &p.prefix,
        }
    }

    pub fn uri_as_str(&self) -> &str {
        match self {
            Namespace::XMLNS => NAMESPACE_URI,
            Namespace::XML => xml::NAMESPACE_URI,
            Namespace::XHTML => xhtml::NAMESPACE_URI,
            //    MathML,
            //    SVG,
            //    U,
            Namespace::Custom(p, _u) => &p.prefix,
        }
    }
}

pub struct NamespaceSet {
    namespace_set: Vec<Namespace>,
}
impl NamespaceSet {
    pub fn new() -> NamespaceSet {
        NamespaceSet {
            namespace_set: vec![Namespace::XMLNS, Namespace::XML],
        }
    }
}
