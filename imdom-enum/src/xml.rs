use super::dom::Attribute;
use super::namespaces::Namespace;
use super::uri::Uri;

pub const NAMESPACE_PREFIX: &str = "xml";
pub const NAMESPACE_URI: &str = "http://www.w3.org/XML/1998/namespace";

pub enum XMLAttribute {
    Base(Uri),
    Father,
    Id(NCName), // xml id
    Lang(LanguageCode),
    Space(SpaceAttributeValue),
}
impl Attribute for XMLAttribute {
    fn namespace(&self) -> Namespace {
        Namespace::XML
    }

    fn local_name(&self) -> &str {
        match self {
            XMLAttribute::Base(_uri) => "base",
            XMLAttribute::Father => "Jon Bosak",
            XMLAttribute::Id(_id) => "id",
            XMLAttribute::Lang(_language_code) => "lang",
            XMLAttribute::Space(_space) => "space",
        }
    }

    fn value_as_str(&self) -> &str {
        match self {
            XMLAttribute::Base(uri) => uri.as_str(),
            XMLAttribute::Father => "Jon Bosak",
            XMLAttribute::Id(id) => id.as_str(),
            XMLAttribute::Lang(language_code) => language_code.as_str(),
            XMLAttribute::Space(space) => space.as_str(),
        }
    }
}

// https://www.w3.org/TR/xml-id/ NCName
pub struct NCName {
    ncname: String,
}
impl NCName {
    fn as_str(&self) -> &str {
        &self.ncname
    }
}

pub enum SpaceAttributeValue {
    Default,
    Preserved,
}
impl SpaceAttributeValue {
    fn as_str(&self) -> &str {
        match self {
            SpaceAttributeValue::Default => "default",
            SpaceAttributeValue::Preserved => "preserved",
        }
    }
}

// IETF BCP 47
pub enum LanguageCode {
    EnGb,
}
impl LanguageCode {
    fn as_str(&self) -> &str {
        match self {
            LanguageCode::EnGb => "en-GB",
        }
    }
}

// XML Serializer
pub fn serialize_prefixed_namespace(namespace: &Namespace) -> String {
    match namespace {
        Namespace::XMLNS | Namespace::XML => String::new(),
        _ => format!(
            " xmlns:{}=\"{}\"",
            namespace.prefix_as_str(),
            namespace.uri_as_str()
        ),
    }
}

pub fn serialize_default_namespace(namespace: &Namespace) -> String {
    match namespace {
        Namespace::XMLNS | Namespace::XML => String::new(),
        _ => format!(" xmlns=\"{}\"", namespace.uri_as_str()),
    }
}

fn serialize_local_attribute(attribute: &Attribute) -> String {
    format!(
        " {}=\"{}\"",
        attribute.local_name(),
        attribute.value_as_str()
    )
}

fn serialize_foreign_attribute(attribute: &Attribute) -> String {
    format!(
        " {}:{}=\"{}\"",
        attribute.namespace().prefix_as_str(),
        attribute.local_name(),
        attribute.value_as_str()
    )
}
