pub struct Uri {
    uri: String,
}
impl Uri {
    pub fn new(uri: String) -> Uri {
        // TODO add checking to make sure uri is correctly formed
        Uri { uri }
    }

    pub fn as_str(&self) -> &str {
        &self.uri
    }
}
