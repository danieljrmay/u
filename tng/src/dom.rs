pub trait Element {
    fn namespace_URI(&self) -> DOMString;
    fn prefix(&self) -> DOMString;
    fn local_name(&self) -> DOMString;
    fn attributes(&self) -> NamedNodeMap;
    /*
    https://www.w3.org/TR/dom/#interface-element

    [Exposed=Window]
    interface Element : Node {
      DONE readonly attribute DOMString? namespaceURI;
      DONE readonly attribute DOMString? prefix;
      DONE readonly attribute DOMString localName;
      readonly attribute DOMString tagName;

               attribute DOMString id;
               attribute DOMString className;
      [SameObject] readonly attribute DOMTokenList classList;

      [SameObject] readonly attribute NamedNodeMap attributes;
      DOMString? getAttribute(DOMString name);
      DOMString? getAttributeNS(DOMString? namespace, DOMString localName);
      void setAttribute(DOMString name, DOMString value);
      void setAttributeNS(DOMString? namespace, DOMString name, DOMString value);
      void removeAttribute(DOMString name);
      void removeAttributeNS(DOMString? namespace, DOMString localName);
      boolean hasAttribute(DOMString name);
      boolean hasAttributeNS(DOMString? namespace, DOMString localName);


      HTMLCollection getElementsByTagName(DOMString localName);
      HTMLCollection getElementsByTagNameNS(DOMString? namespace, DOMString localName);
      HTMLCollection getElementsByClassName(DOMString classNames);
    };
    */
}

pub struct DOMString {
    dom_string: String,
}
impl DOMString {
    fn new(dom_string: String) -> DOMString {
        DOMString { dom_string }
    }
}

pub struct NamedNodeMap {}
