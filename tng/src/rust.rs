use super::dom::DOMString;
use super::dom::Element;
use super::dom::NamedNodeMap;
use super::u::Natural;
use super::u::Natural32;

trait RustElement: Element {
    fn namespace_URI(&self) -> DOMString {}

    fn prefix(&self) -> DOMString {}
}

struct RustU32 {
    value: u32,
}
impl Natural32 for RustU32 {}
impl Natural for RustU32 {}
impl Element for RustU32 {
    fn namespace_URI(&self) -> DOMString {}

    fn prefix(&self) -> DOMString {}

    fn local_name(&self) -> DOMString {}

    fn attributes(&self) -> NamedNodeMap {}
}
