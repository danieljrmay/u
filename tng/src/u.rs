// Abstract

pub trait Natural {}
// Concrete-ish
pub trait Natural32: Natural {}
// Concrete
pub trait Natural32LE: Natural32 {}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

trait NaturalAddition {
    fn lhs(&self) -> Natural;
    fn rhs(&self) -> Natural;
}

trait Execute<T> {
    fn execute(&self) -> &T;
}

struct OverflowError {}

// TODO
// Have number trait which describes a bunch of Number related stuff
// Have a binary operator trait which describes lhs, rhs, execute, etc.
