pub trait MathML {
    fn serialized_content_mathml(&self) -> String {
        format!(
            "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">{}</math>",
            self.serialized_content_mathml_fragment()
        )
    }

    fn serialized_parallel_mathml(&self) -> String {
        format!(
            "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">{}</math>",
            self.serialized_parallel_mathml_fragment()
        )
    }

    fn serialized_presentation_mathml(&self) -> String {
        format!(
            "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">{}</math>",
            self.serialized_presentation_mathml_fragment()
        )
    }

    fn serialized_content_mathml_fragment(&self) -> String;

    fn serialized_parallel_mathml_fragment(&self) -> String {
        format!(
            "<semantics>{}<annotation-xml encoding=\"MathML-Content\">{}</annotation-xml></semantics>",
            self.serialized_presentation_mathml_fragment(),
            self.serialized_content_mathml_fragment()
        )
    }

    fn serialized_presentation_mathml_fragment(&self) -> String;
}
