use super::mathml::MathML;

trait Set: MathML {
    // symbol should be A, B, C, N, P, Z, Q, R, C
    //fn isMemberOf(e: Element) -> bool;
}
struct VecSet {
    elements: Vec<Box<Element>>,
}
impl VecSet {
    fn new(elements: Vec<Box<Element>>) -> VecSet {
        VecSet { elements }
    }
}
impl Set for VecSet {}
impl MathML for VecSet {
    fn serialized_content_mathml_fragment(&self) -> String {
        let mut xml = String::new();

        for element in &self.elements {
            xml.push_str(&element.serialized_content_mathml_fragment());
        }

        format!("<set>{}</set>", xml)
    }

    fn serialized_presentation_mathml_fragment(&self) -> String {
        let mut xml = String::new();

        for element in &self.elements {
            xml.push_str(&element.serialized_presentation_mathml_fragment());
            xml.push_str("<mo>,</mo>");
        }

        format!("<mrow><mo>{{</mo>{}<mo>}}</mo></mrow>", xml)
    }
}

trait Element: MathML {}
trait Integer: Element {
    fn as_i64(&self) -> i64;
}
struct I64Integer {
    value: i64,
}
impl I64Integer {
    fn new(value: i64) -> I64Integer {
        I64Integer { value }
    }
}
impl Integer for I64Integer {
    fn as_i64(&self) -> i64 {
        self.value
    }
}
impl Element for I64Integer {}
impl MathML for I64Integer {
    fn serialized_content_mathml_fragment(&self) -> String {
        format!("<cn type=\"integer\">{}</cn>", self.value)
    }

    fn serialized_presentation_mathml_fragment(&self) -> String {
        format!("<mn>{}</mn>", self.value)
    }
}

struct SetInclusion {
    element: Box<Element>,
    set: Box<Set>,
}
impl SetInclusion {
    fn new(element: Box<Element>, set: Box<Set>) -> SetInclusion {
        SetInclusion { element, set }
    }
}
impl MathML for SetInclusion {
    fn serialized_content_mathml_fragment(&self) -> String {
        format!(
            "<apply><in/>{}{}</apply>",
            &self.element.serialized_content_mathml_fragment(),
            &self.set.serialized_content_mathml_fragment(),
        )
    }

    fn serialized_presentation_mathml_fragment(&self) -> String {
        format!(
            "<mrow>{}<mo>∈</mo>{}</mrow>",
            &self.element.serialized_content_mathml_fragment(),
            &self.set.serialized_content_mathml_fragment(),
        )
    }
}

struct SetExclusion {
    element: Box<Element>,
    set: Box<Set>,
}
impl SetExclusion {
    fn new(element: Box<Element>, set: Box<Set>) -> SetExclusion {
        SetExclusion { element, set }
    }
}
impl MathML for SetExclusion {
    fn serialized_content_mathml_fragment(&self) -> String {
        format!(
            "<apply><notin/>{}{}</apply>",
            &self.element.serialized_content_mathml_fragment(),
            &self.set.serialized_content_mathml_fragment(),
        )
    }

    fn serialized_presentation_mathml_fragment(&self) -> String {
        format!(
            "<mrow>{}<mo>∉</mo>{}</mrow>",
            &self.element.serialized_content_mathml_fragment(),
            &self.set.serialized_content_mathml_fragment(),
        )
    }
}

/* Sets are usually denoted by captial letters: A, B, X, Y, ...
Elemetns are usualayy denoted by lower-case letters: a, b, c, d, e...


Two ways to define a set:
1. List the members A = {a, e, i, o, u}
the S={x, y} syntax is sometimes called the "Tabular form" of a set.

2. State the properties which characterize the elements in the set e.g.

B={ x : x is an even integer, x > 0} reads "B is the set of x such that x is an even integer and x0."


This is called the "set-builder form" or "property method" of specifying a set.


A = B if both have the same elements.
A != B negation

Not that a set does not depend on the order or its elements or if those elements are repeated e.g.
{1,2,2,1,6/3}={1,2}

p is_member(Set)
p is_not_member(Set)

Sometimes we use ellipsis and hope that people know what we mean:

B={2, 4, 6, …}

If E={x: x^2-3x+2=0} then E consists of the solutions of the equation.
Sometimes called the "solution set" of the equation. So E={1,2}.

Number sets:
N={0,1,2,3,…} (non-negative integers)
P={1,2,3,…} {positive integers}
Z={…,-2,-1,0,1,2,…} (integers)
Q=rationals
R=reals
C=complex
H=quaternions
O=octernions
*/

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn i64_integer() {
        let i = I64Integer::new(42);
        assert_eq!(i.as_i64(), 42);
        assert_eq!(
            i.serialized_content_mathml_fragment(),
            "<cn type=\"integer\">42</cn>"
        );
        assert_eq!(i.serialized_presentation_mathml_fragment(), "<mn>42</mn>");
        assert_eq!(
            i.serialized_parallel_mathml(),
            "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mn>42</mn><annotation-xml encoding=\"MathML-Content\"><cn type=\"integer\">42</cn></annotation-xml></semantics></math>"
        );
    }

    #[test]
    fn vec_set() {
        let s = VecSet::new(vec![
            Box::new(I64Integer::new(42)),
            Box::new(I64Integer::new(7)),
            Box::new(I64Integer::new(12)),
        ]);

        assert_eq!(
            s.serialized_content_mathml_fragment(),
            "<set><cn type=\"integer\">42</cn><cn type=\"integer\">7</cn><cn type=\"integer\">12</cn></set>"
        );
        assert_eq!(
            s.serialized_presentation_mathml_fragment(),
            "<mrow><mo>{</mo><mn>42</mn><mo>,</mo><mn>7</mn><mo>,</mo><mn>12</mn><mo>,</mo><mo>}</mo></mrow>"
        );
        assert_eq!(
            s.serialized_parallel_mathml(),
            "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mo>{</mo><mn>42</mn><mo>,</mo><mn>7</mn><mo>,</mo><mn>12</mn><mo>,</mo><mo>}</mo></mrow><annotation-xml encoding=\"MathML-Content\"><set><cn type=\"integer\">42</cn><cn type=\"integer\">7</cn><cn type=\"integer\">12</cn></set></annotation-xml></semantics></math>"
        );
    }

    #[test]
    fn set_inclusion() {
        let s = VecSet::new(vec![
            Box::new(I64Integer::new(42)),
            Box::new(I64Integer::new(7)),
            Box::new(I64Integer::new(12)),
        ]);
        let inclusion = SetInclusion::new(Box::new(I64Integer::new(42)), Box::new(s));

        assert_eq!(
            inclusion.serialized_content_mathml_fragment(),
            "<apply><in/><cn type=\"integer\">42</cn><set><cn type=\"integer\">42</cn><cn type=\"integer\">7</cn><cn type=\"integer\">12</cn></set></apply>"
        );
        assert_eq!(
            inclusion.serialized_presentation_mathml_fragment(),
            "<mrow><cn type=\"integer\">42</cn><mo>∈</mo><set><cn type=\"integer\">42</cn><cn type=\"integer\">7</cn><cn type=\"integer\">12</cn></set></mrow>"
        );
        assert_eq!(
            inclusion.serialized_parallel_mathml(),
            "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><cn type=\"integer\">42</cn><mo>∈</mo><set><cn type=\"integer\">42</cn><cn type=\"integer\">7</cn><cn type=\"integer\">12</cn></set></mrow><annotation-xml encoding=\"MathML-Content\"><apply><in/><cn type=\"integer\">42</cn><set><cn type=\"integer\">42</cn><cn type=\"integer\">7</cn><cn type=\"integer\">12</cn></set></apply></annotation-xml></semantics></math>"
        );
    }

    #[test]
    fn set_exclusion() {
        let s = VecSet::new(vec![
            Box::new(I64Integer::new(42)),
            Box::new(I64Integer::new(7)),
            Box::new(I64Integer::new(12)),
        ]);
        let exclusion = SetExclusion::new(Box::new(I64Integer::new(41)), Box::new(s));

        assert_eq!(
            exclusion.serialized_content_mathml_fragment(),
            "<apply><notin/><cn type=\"integer\">41</cn><set><cn type=\"integer\">42</cn><cn type=\"integer\">7</cn><cn type=\"integer\">12</cn></set></apply>"
        );
        assert_eq!(
            exclusion.serialized_presentation_mathml_fragment(),
            "<mrow><cn type=\"integer\">41</cn><mo>∉</mo><set><cn type=\"integer\">42</cn><cn type=\"integer\">7</cn><cn type=\"integer\">12</cn></set></mrow>"
        );
        assert_eq!(
            exclusion.serialized_parallel_mathml(),
            "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><cn type=\"integer\">41</cn><mo>∉</mo><set><cn type=\"integer\">42</cn><cn type=\"integer\">7</cn><cn type=\"integer\">12</cn></set></mrow><annotation-xml encoding=\"MathML-Content\"><apply><notin/><cn type=\"integer\">41</cn><set><cn type=\"integer\">42</cn><cn type=\"integer\">7</cn><cn type=\"integer\">12</cn></set></apply></annotation-xml></semantics></math>"
        );
    }

}
