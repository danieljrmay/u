pub mod u;

struct Dataspace {
    prefix: &'static str,
    uri: &'static str,
}
impl Dataspace {
    fn prefix(&self) -> &str {
        self.prefix
    }

    fn uri(&self) -> &str {
        self.uri
    }
}

trait Datatype {
    fn name(&self) -> &str;
    fn dataspace(&self) -> &Dataspace;
    fn read(&self) -> &str;
    // write
    // execute
    // copy
    // dereference
    // cast
}
