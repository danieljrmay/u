use super::Dataspace;
use super::Datatype;

const U_DATASPACE: Dataspace = Dataspace {
    prefix: "u",
    uri: "https://danieljrmay.com/U",
};

struct Natural32 {
    value: u32,
    value_as_string: String,
}
impl Natural32 {
    fn new(value: u32) -> Natural32 {
        Natural32 {
            value,
            value_as_string: value.to_string(),
        }
    }
}
impl Datatype for Natural32 {
    fn name(&self) -> &str {
        "natural32"
    }

    fn dataspace(&self) -> &Dataspace {
        &U_DATASPACE
    }

    fn read(&self) -> &str {
        &self.value_as_string
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn natural32() {
        let n = Natural32::new(42);
        assert_eq!(n.name(), "natural32");
        assert_eq!(n.read(), "42");
        assert_eq!(n.dataspace().prefix(), "u");
        assert_eq!(n.dataspace().uri(), "https://danieljrmay.com/U");
    }
}
