extern crate wasm_bindgen;
extern crate web_sys;

use imdom::dom::Node;
use imdom::dom::Serializer;
use imdom::namespaces::NamespaceRegistry;
use imdom::uexpr_token_parser;
use uexpr::tokenizer;

use wasm_bindgen::prelude::*;
//use web_sys::Document;
//use web_sys::HtmlElement;
//use web_sys::Window;

// Called when the wasm module is instantiated
#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    // Use `web_sys`'s global `window` function to get a handle on the global
    // window object.

    Ok(())
}

#[wasm_bindgen]
pub fn add(a: u32, b: u32) -> u32 {
    a + b
}

#[wasm_bindgen]
pub fn parse(sexpr: &str) -> String {
    let namespace_registry = NamespaceRegistry::new();
    let default_namespace = namespace_registry.get("xhtml").unwrap();
    let serializer = Serializer::new();

    match tokenizer::tokenize(sexpr) {
        Ok(tokens) => {
            println!("Tokens={:?}", tokens);
            let node: Node = uexpr_token_parser::parse_tokens(
                &namespace_registry,
                default_namespace,
                &tokens[..],
            );

            return serializer.serialize(default_namespace, node);
        }
        Err(tokenizing_error) => {
            return format!("Error tokenizing input: {:?}", tokenizing_error);
        }
    }
}
