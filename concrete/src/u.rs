const U_NAMESPACE_PREFIX: &str = "u";
const U_NAMESPACE_URI: &str = "https://danieljrmay.com/U";

trait DataType {}
trait UElement: Element {}

struct Natural32 {
    value: u32,
}
impl Natural32 {
    fn new(value: u32) -> Natural32 {
        Natural32 { value }
    }
}
impl DataType for Natural32 {}
impl UElement for Natural32 {}
impl Element for Natural32 {
    fn local_name(&self) -> &str {
        "natural32"
    }
    fn namespace_URI(&self) -> &str {
        U_NAMESPACE_URI
    }

    fn prefix(&self) -> &str {
        U_NAMESPACE_PREFIX
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn natural32() {
        let n = Natural32::new(42);
        assert!(n.attributes().is_none());
        assert_eq!(n.local_name(), "natural32");
        assert_eq!(n.namespace_URI(), U_NAMESPACE_URI);
        assert_eq!(n.prefix(), U_NAMESPACE_PREFIX);
    }
}

// Elsewhere
trait Element {
    fn attributes(&self) -> Option<&NodeNameMap> {
        None
    }

    fn local_name(&self) -> &str;
    fn namespace_URI(&self) -> &str;
    fn prefix(&self) -> &str;
}

trait Attribute {}

#[derive(Debug)]
struct NodeNameMap {}
impl NodeNameMap {
    fn new() -> NodeNameMap {
        NodeNameMap {}
    }
}
