pub mod xhtml;

pub struct Namespace<'a> {
    prefix: &'a str,
    uri: &'a str,
}

pub trait Attribute {
    fn namespace(&self) -> &Namespace;
    fn local_name(&self) -> &str;
    fn value(&self) -> &str;
}

pub struct AttributeSet {
    attributes: Option<Vec<Box<Attribute>>>,
}

pub enum Node<'a> {
    Comment(Comment),
    Document(Document<'a>),
    DocumentFragment(DocumentFragment<'a>),
    Element(Box<Element>),
    ProcessingInstruction(ProcessingInstruction),
    Text(Text),
}

pub struct NodeList<'a> {
    nodes: Option<Vec<Node<'a>>>,
}

pub struct Comment {
    comment: String,
}

pub struct Document<'a> {
    root_node: &'a Node<'a>,
}

pub struct DocumentFragment<'a> {
    nodes: NodeList<'a>,
}

pub trait Element {
    fn namespace(&self) -> &Namespace;
    fn local_name(&self) -> &str;
    fn attributes(&self) -> &AttributeSet;
    fn children(&self) -> &NodeList;
}

pub struct ProcessingInstruction {
    target: String, // TODO perhaps make this an enum itself
    data: String,
}

pub struct Text {
    text: String,
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
