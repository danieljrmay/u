use super::Namespace;

const NAMESPACE: Namespace = Namespace {
    prefix: "xhtml",
    uri: "http://",
};
