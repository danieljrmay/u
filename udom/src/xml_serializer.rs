pub enum XMLVersion {
    Version1_0,
    Version1_1,
}

pub struct XMLSerializer {
    version: XMLVersion,
    encoding: &str,
    standalone: bool,
}
impl XMLSerializer {
    pub fn serialize_document(doc: &UDocument) -> String {
        let mut xml = String::new();

        match self.version {
            XMLVersion::Version1_0 => {
                if standalone {
                    xml.push_str(format!(
                        "<?xml version=\"1.0\" encoding=\"{}\" standalone=\"yes\"?>",
                        self.encoding
                    ))
                } else {
                    xml.push_str(format!(
                        "<?xml version=\"1.0\" encoding=\"{}\" standalone=\"no\"?>",
                        self.encoding
                    ))
                }
            }
            XMLVersion::Version1_1 => {
                if standalone {
                    xml.push_str(format!(
                        "<?xml version=\"1.1\" encoding=\"{}\" standalone=\"yes\"?>",
                        self.encoding
                    ))
                } else {
                    xml.push_str(format!(
                        "<?xml version=\"1.1\" encoding=\"{}\" standalone=\"no\"?>",
                        self.encoding
                    ))
                }
            }
        }

        xml
    }

    pub fn serialize_fragment(doc: &UDocumentFragment) -> String {
        let mut xml = String::new();

        xml
    }
}
