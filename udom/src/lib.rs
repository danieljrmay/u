enum UNamespace {
    MathML,
    UMath,
}
impl UNamespace {
    fn prefix_as_str(&self) -> &str {
        match self {
            UNamespace::MathML => "mathml",
            UNamespace::UMath => "umath",
        }
    }

    fn uri_as_str(&self) -> &str {
        match self {
            UNamespace::MathML => "http://www.w3.org/1998/Math/MathML",
            UNamespace::UMath => "https://gitlab.com/danieljrmay/u/math",
        }
    }
}

trait UAttribute {
    fn unamespace(&self) -> UNamespace;
    fn local_name(&self) -> &str;
    fn value_as_str(&self) -> &str;
}

trait UObject {
    fn unamespace(&self) -> UNamespace;
    fn local_name(&self) -> &str;
    fn children(&self) -> Vec<Box<UObject>>;
}

pub struct UDocument {
    root: Box<UObject>,
}

pub struct UDocumentFragment {
    children: Vec<Box<UObject>>,
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
