/* tslint:disable */
/**
* @returns {void} 
*/
export function main(): void;
/**
* @param {number} a 
* @param {number} b 
* @returns {number} 
*/
export function add(a: number, b: number): number;
/**
* @param {string} sexpr 
* @returns {string} 
*/
export function parse(sexpr: string): string;
