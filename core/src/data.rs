trait Data {
    fn xml(&self) -> String;
}

trait RData<T>: Data {
    fn read(&self) -> T;
}

trait WData<T>: Data {
    fn write(&self, data: T);
}

trait RWData<T>: RData<T> + WData<T> {}

trait Reference<T, R>: RWData<T> {
    fn dereference(&self) -> R;
}

trait Code<T, R>: RWData<T> {
    fn operands(&self) -> Vec<Box<Data>>;
    fn execute(&self) -> R;
}

// implementation
