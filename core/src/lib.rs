enum XMLNodeType {
    Attribute,
    Comment,
    Document,
    DocumentFragment,
    Element,
    ProcessingInstruction,
    Text,
}

#[derive(Clone)]
enum DataType {
    U(UDataType),
    XML(XMLDataType),
    XMLNS(XMLNSDataType),
}
impl DataType {
    fn contents(&self) -> Option<&Vec<DataType>> {
        match self {
            DataType::U(t) => t.contents(),
            DataType::XML(t) => t.contents(),
            DataType::XMLNS(t) => t.contents(),
        }
    }

    fn execute(&self) -> DataType {
        match self {
            DataType::U(t) => t.execute(),
            DataType::XML(t) => t.execute(),
            DataType::XMLNS(t) => t.execute(),
        }
    }

    fn local_name(&self) -> &str {
        match self {
            DataType::U(t) => t.local_name(),
            DataType::XML(t) => t.local_name(),
            DataType::XMLNS(t) => t.local_name(),
        }
    }

    fn prefix(&self) -> &str {
        match self {
            DataType::U(_t) => "u",
            DataType::XML(_t) => "xml",
            DataType::XMLNS(_t) => "xmlns",
        }
    }

    fn uri(&self) -> &str {
        match self {
            DataType::U(_t) => "https://danieljrmay.com/U",
            DataType::XML(_t) => "http://www.w3.org/XML/1998/namespace",
            DataType::XMLNS(_t) => "http://www.w3.org/2000/xmlns/",
        }
    }

    fn value_as_str(&self) -> Option<String> {
        match self {
            DataType::U(t) => t.value_as_str(),
            DataType::XML(t) => t.value_as_str(),
            DataType::XMLNS(t) => t.value_as_str(),
        }
    }

    fn xml_node_type(&self) -> XMLNodeType {
        match self {
            DataType::U(t) => t.xml_node_type(),
            DataType::XML(t) => t.xml_node_type(),
            DataType::XMLNS(t) => t.xml_node_type(),
        }
    }
}

#[derive(Clone)]
enum UDataType {
    Add(Vec<DataType>),
    U64(u64),
}
impl UDataType {
    fn contents(&self) -> Option<&Vec<DataType>> {
        match self {
            UDataType::Add(c) => Some(c),
            UDataType::U64(_v) => None,
        }
    }

    fn execute(&self) -> DataType {
        match self {
            UDataType::Add(contents) => match add(contents) {
                Some(result) => {
                    return result;
                }
                None => return DataType::U(self.clone()),
            },
            UDataType::U64(v) => DataType::U(UDataType::U64(*v)),
        }
    }

    fn local_name(&self) -> &str {
        match self {
            UDataType::Add(_c) => "add",
            UDataType::U64(_v) => "u64",
        }
    }

    fn value_as_str(&self) -> Option<String> {
        match self {
            UDataType::Add(_c) => None,
            UDataType::U64(v) => Some(v.to_string()),
        }
    }

    fn xml_node_type(&self) -> XMLNodeType {
        XMLNodeType::Element
    }
}

fn add(operands: &Vec<DataType>) -> Option<DataType> {
    let mut sum = 0;

    for dt in operands {
        match dt {
            DataType::U(udt) => match udt {
                UDataType::U64(value) => sum += value,
                _ => {
                    return None;
                }
            },
            _ => {
                return None;
            }
        }
    }

    Some(DataType::U(UDataType::U64(sum)))
}

#[derive(Clone)]
enum XMLDataType {
    Father,
}
impl XMLDataType {
    fn contents(&self) -> Option<&Vec<DataType>> {
        match self {
            XMLDataType::Father => None,
        }
    }

    fn execute(&self) -> DataType {
        match self {
            XMLDataType::Father => DataType::XML(XMLDataType::Father),
        }
    }

    fn local_name(&self) -> &str {
        match self {
            XMLDataType::Father => "Father",
        }
    }

    fn value_as_str(&self) -> Option<String> {
        match self {
            XMLDataType::Father => Some("Jon Bosak".to_string()),
        }
    }

    fn xml_node_type(&self) -> XMLNodeType {
        match self {
            XMLDataType::Father => XMLNodeType::Attribute,
        }
    }
}

#[derive(Clone)]
enum XMLNSDataType {
    NoIdea,
}
impl XMLNSDataType {
    fn contents(&self) -> Option<&Vec<DataType>> {
        match self {
            XMLNSDataType::NoIdea => None,
        }
    }

    fn execute(&self) -> DataType {
        match self {
            XMLNSDataType::NoIdea => DataType::XMLNS(XMLNSDataType::NoIdea),
        }
    }

    fn local_name(&self) -> &str {
        match self {
            XMLNSDataType::NoIdea => "noidea",
        }
    }

    fn value_as_str(&self) -> Option<String> {
        match self {
            XMLNSDataType::NoIdea => Some("noideaVALUE".to_string()),
        }
    }

    fn xml_node_type(&self) -> XMLNodeType {
        match self {
            XMLNSDataType::NoIdea => XMLNodeType::Comment,
        }
    }
}

struct XMLSerializer {}
impl XMLSerializer {
    pub fn new() -> XMLSerializer {
        XMLSerializer {}
    }

    pub fn serialize(&self, default_namespace: &str, datatype: &DataType) -> String {
        let mut xml = String::new();

        match datatype.xml_node_type() {
            XMLNodeType::Attribute => {
                xml.push_str(&self.serialize_attribute(default_namespace, datatype))
            }
            XMLNodeType::Comment => {
                xml.push_str(&format!("<!--{}-->", datatype.value_as_str().unwrap()))
            }
            XMLNodeType::Document => xml.push_str("DOCUMENT"),
            XMLNodeType::DocumentFragment => xml.push_str("DOCUMENT_FRAGMENT"),
            XMLNodeType::Element => {
                xml.push_str(&self.serialize_element(default_namespace, datatype))
            }
            XMLNodeType::ProcessingInstruction => xml.push_str(&format!(
                "<?{} {}?>",
                datatype.local_name(),
                datatype.value_as_str().unwrap()
            )),
            XMLNodeType::Text => xml.push_str(&datatype.value_as_str().unwrap()),
        }

        xml
    }

    fn serialize_attribute(&self, default_namespace: &str, datatype: &DataType) -> String {
        let attribute_value = if let Some(value) = datatype.value_as_str() {
            value
        } else {
            String::from("true")
        };

        if datatype.uri() == default_namespace {
            //TODO also works for XML and XMLNS namespaces
            return format!(" {}=\"{}\"", datatype.local_name(), attribute_value);
        } else {
            //TODO check that attribute namespace has been defined in the XML
            return format!(
                " {}:{}=\"{}\"",
                datatype.prefix(),
                datatype.local_name(),
                attribute_value
            );
        }
    }

    fn serialize_element(&self, default_namespace: &str, datatype: &DataType) -> String {
        match datatype.contents() {
            Some(contents) => {
                let mut attributes_xml = String::new();
                let mut children_xml = String::new();

                for d in contents {
                    match d.xml_node_type() {
                        XMLNodeType::Attribute => {
                            attributes_xml
                                .push_str(&self.serialize_attribute(default_namespace, d));
                        }
                        XMLNodeType::Comment
                        | XMLNodeType::ProcessingInstruction
                        | XMLNodeType::Text => {
                            // TODO Tidy this and serialize() method
                            children_xml.push_str(&self.serialize(default_namespace, d));
                        }
                        XMLNodeType::Element => {
                            children_xml.push_str(&self.serialize(d.uri(), d));
                        }
                        _ => panic!("SHOULD NOT HAVE THIS DATAtype as contents."),
                    }
                }

                format!(
                    "<{}{}{}>{}</{}>",
                    datatype.local_name(),
                    self.serialize_default_namespace(default_namespace, datatype.uri()),
                    attributes_xml,
                    children_xml,
                    datatype.local_name()
                )
            }
            None => match datatype.value_as_str() {
                Some(value) => format!(
                    "<{}{}>{}</{}>",
                    datatype.local_name(),
                    self.serialize_default_namespace(default_namespace, datatype.uri()),
                    value,
                    datatype.local_name()
                ),
                None => format!(
                    "<{}{}/>",
                    datatype.local_name(),
                    self.serialize_default_namespace(default_namespace, datatype.uri()),
                ),
            },
        }
    }

    fn serialize_default_namespace(&self, default_namespace: &str, namespace: &str) -> String {
        if namespace == default_namespace {
            // TODO also check not using XML or XMLNS namespace
            return String::new();
        } else {
            return format!(" xmlns=\"{}\"", namespace);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn xml_test() {
        let a = DataType::U(UDataType::U64(7));
        assert_eq!(a.local_name(), "u64");

        let serializer = XMLSerializer::new();
        assert_eq!(
            serializer.serialize("", &a),
            "<u64 xmlns=\"https://danieljrmay.com/U\">7</u64>"
        );

        let b = DataType::U(UDataType::U64(5));
        let myadd = DataType::U(UDataType::Add(vec![a, b]));
        assert_eq!(
            serializer.serialize("", &myadd),
            "<add xmlns=\"https://danieljrmay.com/U\"><u64>7</u64><u64>5</u64></add>"
        );

        assert_eq!(myadd.local_name(), "add");
        assert_eq!(myadd.prefix(), "u");

        let c = myadd.execute();
        assert_eq!(
            serializer.serialize("", &c),
            "<u64 xmlns=\"https://danieljrmay.com/U\">12</u64>"
        );
    }

}

/*


    fn write(&mut self, data: u64) {
        self.value = data
    }



pub trait Vertex {
    fn execute(&self) -> Box<Vertex>;
    fn xml(&self) -> String;
}

pub trait Connector {
    fn xml(&self) -> String;
}

pub struct ConnectorList {
    connectors: Vec<Box<Connector>>,
}
impl ConnectorList {
    fn new() -> ConnectorList {
        ConnectorList {
            connectors: Vec::new(),
        }
    }
}
 */
/*
pub trait Natural {
    fn to_usize(&self) -> usize;
}
pub struct RustUSize {
    value: usize,
}
impl RustUSize {
    fn new(value: usize) -> RustUSize {
        RustUSize { value }
    }
}
impl Natural for RustUSize {
    fn to_usize(&self) -> usize {
        self.value
    }
}
impl Vertex for RustUSize {
    fn execute(&self) -> Box<Vertex> {
        Box::new(RustUSize::new(self.value))
    }

    fn xml(&self) -> String {
        format!("<natural>{}</natural>", self.value)
    }
}

pub enum UVertex {
    Natural(usize),
    Add(ConnectorList),
}



trait Data {
    fn contents(&self) -> Option<Vec<Box<Data>>>;
    fn execute(&self) -> Box<Data>;
}

enum DataType {
    Attribute,
    Comment,
    Composite,
    Operator,
    Primitive,
    Text,
}

// implementation
struct U64 {
    value: u64,
}
impl U64 {
    fn new(value: u64) -> U64 {
        U64 { value }
    }
}
impl Data for U64 {
    fn contents(&self) -> Option<Vec<Box<Data>>> {
        None
    }

    fn datatype(&self) -> DataType {
        DataType::Primitive
    }

    fn execute(&self) -> Box<Data> {
        Box::new(U64::new(self.value))
    }

    fn name(&self) -> &str {
        "u64"
    }

    fn space(&self) -> Space {
        Space::U
    }

    fn value_as_string(&self) -> Option<String> {
        Some(format!("{}", self.value))
    }
}

struct AddU64 {
    operands: Vec<Box<Data>>,
}
impl Data for AddU64 {
    fn contents(&self) -> Option<Vec<Box<Data>>> {
        Some(self.operands)
    }

    fn datatype(&self) -> DataType {
        DataType::Operator
    }

    fn execute(&self) -> Box<Data> {
        Box::new(U64::new(self.value))
    }

    fn name(&self) -> &str {
        "addu64"
    }

    fn space(&self) -> Space {
        Space::U
    }

    fn value_as_string(&self) -> Option<String> {
        None
    }
}

trait RData<T>: Data {
    fn read(&self) -> T;
}

trait WData<T>: Data {
    fn write(&mut self, data: T);
}

trait RWData<T>: RData<T> + WData<T> {}

trait Reference<T, R>: RWData<T> {
    fn dereference(&self) -> R;
}

trait Code<T, R>: RWData<T> {
    fn operands(&self) -> Vec<Box<Data>>;
    fn execute(&self) -> R;
}

*/

//Composite
