.PHONY: all
all:
	cargo +nightly build --target wasm32-unknown-unknown
	wasm-bindgen target/wasm32-unknown-unknown/debug/imdom_wasm.wasm --target web --out-dir ./src

.PHONY: serve
serve:
	cd src && python3 -m http.server

.PHONY: clean
clean:
	rm -rf src/imdom_wasm*
	rm -rf target
