use imdom_trait::dom::AttributeSet;
use imdom_trait::dom::Node;
use imdom_trait::dom::NodeList;
use imdom_trait::namespace;
use imdom_trait::xhtml;
use imdom_trait::xhtml::attribute::Title;
use imdom_trait::xhtml::element::Paragraph;
use imdom_trait::xml_serializer;

fn main() {
    println!("imdom-trait test");

    let default_namespace = &xhtml::XHTML_NAMESPACE;

    let my_comment = Node::new_comment("This is my comment.".to_string());
    println!(
        "{}",
        xml_serializer::serialize(default_namespace, &my_comment)
    );

    let my_text = Node::new_text("This is my text.".to_string());
    println!("{}", xml_serializer::serialize(default_namespace, &my_text));

    let my_title_attr = Title::new("Hello this is my tooltip!".to_string());
    println!(
        "{}",
        xml_serializer::serialize_native_attribute(&my_title_attr)
    );
    println!(
        "{}",
        xml_serializer::serialize_foreign_attribute(&my_title_attr)
    );

    let my_p = Paragraph::new(AttributeSet::new(), NodeList::new());
    println!(
        "{}",
        xml_serializer::serialize(&namespace::XMLNS_NAMESPACE, &my_p)
    );
}
