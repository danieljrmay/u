use super::namespace::Namespace;

pub trait Attribute {
    fn namespace(&self) -> &Namespace;
    fn local_name(&self) -> &str;
    fn value(&self) -> &str;
}

pub struct AttributeSet {
    attributes: Vec<Box<Attribute>>,
}
impl AttributeSet {
    pub fn new() -> AttributeSet {
        AttributeSet { attributes: vec![] }
    }

    pub fn add(&mut self, boxed_attribute: Box<Attribute>) {
        // TODO check does not already exist etc.
        self.attributes.push(boxed_attribute);
    }

    pub fn has_attributes(&self) -> bool {
        if self.attributes.len() > 0 {
            return true;
        } else {
            return false;
        }
    }
}

pub enum Node {
    Comment(Comment),
    //Document,
    //DocumentFragment,
    Element(Box<Element>),
    //ProcessingInstruction,
    Text(Text),
}
impl Node {
    pub fn new_comment(comment: String) -> Node {
        Node::Comment(Comment::new(comment))
    }

    pub fn new_text(text: String) -> Node {
        Node::Text(Text::new(text))
    }

    pub fn new_element(boxed_element: Box<Element>) -> Node {
        Node::Element(boxed_element)
    }
}

pub struct NodeList {
    nodes: Vec<Node>,
}
impl NodeList {
    pub fn new() -> NodeList {
        NodeList { nodes: vec![] }
    }

    pub fn add(&mut self, node: Node) {
        self.nodes.push(node);
    }
}

pub struct Comment {
    comment: String,
}
impl Comment {
    pub fn new(comment: String) -> Comment {
        // TODO check validity i.e. no "--"
        Comment { comment }
    }

    pub fn value(&self) -> &str {
        &self.comment
    }
}

pub trait Element {
    fn namespace(&self) -> &Namespace;
    fn local_name(&self) -> &str;
    fn attributes(&self) -> &AttributeSet;
    fn children(&self) -> &NodeList;
}

pub struct Text {
    text: String,
}
impl Text {
    pub fn new(text: String) -> Text {
        // TODO check validity
        Text { text }
    }

    pub fn value(&self) -> &str {
        &self.text
    }
}
