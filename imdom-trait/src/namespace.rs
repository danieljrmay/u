use std::cmp::PartialEq;

pub struct Namespace {
    pub prefix: &'static str,
    pub uri: &'static str,
}
impl PartialEq for Namespace {
    fn eq(&self, other: &Namespace) -> bool {
        self.uri == other.uri
    }
}
impl Namespace {
    pub fn new(prefix: &'static str, uri: &'static str) -> Namespace {
        //TODO implement syntax checking
        Namespace { prefix, uri }
    }
}

pub const XMLNS_NAMESPACE: Namespace = Namespace {
    prefix: "xmlns",
    uri: "http://www.w3.org/2000/xmlns/",
};
