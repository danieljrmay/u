use super::namespace::Namespace;

const XML_NAMESPACE: Namespace = Namespace {
    prefix: "xml",
    uri: "http://www.w3.org/XML/1998/namespace",
};
