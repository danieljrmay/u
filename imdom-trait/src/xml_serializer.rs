use super::dom::Attribute;
use super::dom::Node;
use super::namespace::Namespace;

pub fn serialize(default_namespace: &Namespace, node: &Node) -> String {
    let mut xml = String::new();

    match node {
        Node::Comment(c) => {
            xml.push_str(&format!("<!--{}-->", c.value()));
        }
        Node::Element(e) => {
            if default_namespace == e.namespace() {
                xml.push_str(&format!("<{}/>", e.local_name()));
            } else {
                xml.push_str(&format!(
                    "<{} xmlns=\"{}\"/>",
                    e.local_name(),
                    e.namespace().uri
                ));
            }
        }
        Node::Text(t) => {
            xml.push_str(t.value());
        }
    }

    xml
}

pub fn serialize_native_attribute(attribute: &Attribute) -> String {
    format!(" {}=\"{}\"", attribute.local_name(), attribute.value())
}

pub fn serialize_foreign_attribute(attribute: &Attribute) -> String {
    format!(
        " {}:{}=\"{}\"",
        attribute.namespace().prefix,
        attribute.local_name(),
        attribute.value()
    )
}
