use super::dom::AttributeSet;
use super::dom::Element;
use super::dom::Node;
use super::dom::NodeList;
use super::namespace::Namespace;

pub const XHTML_NAMESPACE: Namespace = Namespace {
    prefix: "xhtml",
    uri: "http://www.w3.org/1999/xhtml",
};

pub mod attribute {
    use super::super::dom::Attribute;
    use super::super::namespace::Namespace;
    use super::XHTML_NAMESPACE;

    pub struct Title {
        value: String,
    }
    impl Attribute for Title {
        fn namespace(&self) -> &Namespace {
            &XHTML_NAMESPACE
        }

        fn local_name(&self) -> &str {
            "title"
        }

        fn value(&self) -> &str {
            &self.value
        }
    }
    impl Title {
        pub fn new(value: String) -> Title {
            Title { value }
        }
    }
}

pub mod element {
    use super::super::dom::AttributeSet;
    use super::super::dom::Element;
    use super::super::dom::Node;
    use super::super::dom::NodeList;
    use super::super::namespace::Namespace;
    use super::XHTML_NAMESPACE;

    pub struct Paragraph {
        attributes: AttributeSet,
        children: NodeList,
    }
    impl Element for Paragraph {
        fn namespace(&self) -> &Namespace {
            &XHTML_NAMESPACE
        }

        fn local_name(&self) -> &str {
            "p"
        }

        fn attributes(&self) -> &AttributeSet {
            &self.attributes
        }

        fn children(&self) -> &NodeList {
            &self.children
        }
    }
    impl Paragraph {
        pub fn new(attributes: AttributeSet, children: NodeList) -> Node {
            Node::new_element(Box::new(Paragraph {
                attributes,
                children,
            }))
        }
    }
}
