pub mod dom;
pub mod namespace;
pub mod xhtml;
pub mod xml_serializer;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
