#[derive(Debug)]
enum Mode {
    Start,
    ScanningValueBool,
    ScanningValueString,
    ScanningNumber,
    ScanningNegativeNumber,
    ScanningFloat,
    ScanningElementName,
    ScanningElementArguments,
    ScanningAttributeName,
    ScanningAttributeArguments,
}

struct ModeStack {
    stack: Vec<Mode>,
}

impl ModeStack {
    fn new() -> ModeStack {
        ModeStack {
            stack: vec![Mode::Start],
        }
    }

    fn peek(&self) -> &Mode {
        let peek_index = self.stack.len() - 1;
        let current_mode: &Mode = &self.stack[peek_index];
        current_mode
    }

    fn pop(&mut self) -> Mode {
        self.stack.pop().expect("Error popping ModeStack.")
    }

    fn push(&mut self, mode: Mode) {
        self.stack.push(mode);
    }

    fn replace(&mut self, mode: Mode) {
        self.stack.pop();
        self.stack.push(mode);
    }
}

#[derive(Debug, PartialEq)]
pub enum Token {
    ElementStart,
    ElementPrefix(String),
    ElementLocalName(String),
    ElementEnd,
    AttributeStart,
    AttributePrefix(String),
    AttributeLocalName(String),
    AttributeEnd,
    ValueBool(bool),
    ValueUInt(u64),
    ValueInt(i64),
    ValueFloat(f64),
    ValueString(String),
}

#[derive(Debug)]
pub struct TokenizeError {
    mode: Mode,
    current_char: char,
    current_token: String,
    tokens: Vec<Token>,
    message: String,
}

impl TokenizeError {
    fn new(
        mode: Mode,
        current_char: char,
        current_token: String,
        tokens: Vec<Token>,
        message: String,
    ) -> TokenizeError {
        TokenizeError {
            mode,
            current_char,
            current_token,
            tokens,
            message,
        }
    }
}

const ELEMENT_START: char = '[';
const ELEMENT_END: char = ']';
const ATTRIBUTE_START: char = '{';
const ATTRIBUTE_END: char = '}';
const BOOLEAN_TRUE: char = 't';
const BOOLEAN_FALSE: char = 'f';
const STRING_START: char = '"';
const STRING_END: char = '"';
const ZERO: char = '0';
const NINE: char = '9';
const MINUS: char = '-';
const DECIMAL_POINT: char = '.';
const SPACE: char = ' ';
const LINE_FEED: char = '\n';
const NAMESPACE_SEPARATOR: char = ':';

pub fn tokenize(raw_uexpr: &str) -> Result<Vec<Token>, TokenizeError> {
    let uexpr: String = raw_uexpr.to_owned();

    // TODO use string slice rather than owned uexpr

    let mut tokens: Vec<Token> = Vec::new();
    let mut mode_stack: ModeStack = ModeStack::new();
    let mut token_begin = 0;

    for (i, c) in uexpr.chars().enumerate() {
        match mode_stack.peek() {
            Mode::Start => match c {
                SPACE | LINE_FEED => continue,
                ELEMENT_START => {
                    mode_stack.push(Mode::ScanningElementName);
                    tokens.push(Token::ElementStart);
                    token_begin = i + 1;
                }
                STRING_START => {
                    mode_stack.push(Mode::ScanningValueString);
                    token_begin = i + 1;
                }
                ZERO...NINE => {
                    mode_stack.push(Mode::ScanningNumber);
                    token_begin = i;
                }
                MINUS => {
                    mode_stack.push(Mode::ScanningNegativeNumber);
                    token_begin = i;
                }
                DECIMAL_POINT => {
                    mode_stack.push(Mode::ScanningFloat);
                    token_begin = i;
                }
                BOOLEAN_FALSE | BOOLEAN_TRUE => {
                    mode_stack.push(Mode::ScanningValueBool);
                    token_begin = i;
                }
                _ => {
                    return Result::Err(TokenizeError::new(
                        mode_stack.pop(),
                        c,
                        uexpr[token_begin..i].to_owned(),
                        tokens,
                        "Illegal character detected in Mode::Start.".to_owned(),
                    ))
                }
            },
            Mode::ScanningElementName => match c {
                'a'...'z' => continue,
                ELEMENT_END => {
                    tokens.push(Token::ElementLocalName(uexpr[token_begin..i].to_owned()));
                    tokens.push(Token::ElementEnd);
                    mode_stack.pop();
                }
                SPACE | LINE_FEED => {
                    tokens.push(Token::ElementLocalName(uexpr[token_begin..i].to_owned()));
                    mode_stack.replace(Mode::ScanningElementArguments);
                }
                NAMESPACE_SEPARATOR => {
                    tokens.push(Token::ElementPrefix(uexpr[token_begin..i].to_owned()));
                    token_begin = i + 1;
                }
                ELEMENT_START => {
                    tokens.push(Token::ElementLocalName(uexpr[token_begin..i].to_owned()));
                    mode_stack.replace(Mode::ScanningElementArguments);

                    mode_stack.push(Mode::ScanningElementName);
                    tokens.push(Token::ElementStart);
                    token_begin = i + 1;
                }
                ATTRIBUTE_START => {
                    tokens.push(Token::ElementLocalName(uexpr[token_begin..i].to_owned()));
                    mode_stack.replace(Mode::ScanningElementArguments);

                    mode_stack.push(Mode::ScanningAttributeName);
                    tokens.push(Token::AttributeStart);
                    token_begin = i + 1;
                }
                STRING_START => {
                    tokens.push(Token::ElementLocalName(uexpr[token_begin..i].to_owned()));
                    mode_stack.replace(Mode::ScanningElementArguments);

                    mode_stack.push(Mode::ScanningValueString);
                    token_begin = i + 1;
                }
                _ => {
                    return Result::Err(TokenizeError::new(
                        mode_stack.pop(),
                        c,
                        uexpr[token_begin..i].to_owned(),
                        tokens,
                        "Illegal character detected in Mode::ScanningElementName.".to_owned(),
                    ))
                }
            },
            Mode::ScanningElementArguments => match c {
                SPACE | LINE_FEED => continue,
                ELEMENT_END => {
                    tokens.push(Token::ElementEnd);
                    mode_stack.pop();
                }
                ELEMENT_START => {
                    mode_stack.push(Mode::ScanningElementName);
                    tokens.push(Token::ElementStart);
                    token_begin = i + 1;
                }
                ATTRIBUTE_START => {
                    mode_stack.push(Mode::ScanningAttributeName);
                    tokens.push(Token::AttributeStart);
                    token_begin = i + 1;
                }
                STRING_START => {
                    mode_stack.push(Mode::ScanningValueString);
                    token_begin = i + 1;
                }
                ZERO...NINE => {
                    mode_stack.push(Mode::ScanningNumber);
                    token_begin = i;
                }
                MINUS => {
                    mode_stack.push(Mode::ScanningNegativeNumber);
                    token_begin = i;
                }
                DECIMAL_POINT => {
                    mode_stack.push(Mode::ScanningFloat);
                    token_begin = i;
                }
                BOOLEAN_FALSE | BOOLEAN_TRUE => {
                    mode_stack.push(Mode::ScanningValueBool);
                    token_begin = i;
                }
                _ => {
                    return Result::Err(TokenizeError::new(
                        mode_stack.pop(),
                        c,
                        uexpr[token_begin..i].to_owned(),
                        tokens,
                        "Illegal character detected in Mode::ScanningElementArguments.".to_owned(),
                    ))
                }
            },
            Mode::ScanningAttributeName => match c {
                'a'...'z' => continue,
                SPACE | LINE_FEED => {
                    tokens.push(Token::AttributeLocalName(uexpr[token_begin..i].to_owned()));
                    mode_stack.replace(Mode::ScanningAttributeArguments);
                }
                NAMESPACE_SEPARATOR => {
                    tokens.push(Token::AttributePrefix(uexpr[token_begin..i].to_owned()));
                    token_begin = i + 1;
                }
                ATTRIBUTE_END => {
                    tokens.push(Token::AttributeLocalName(uexpr[token_begin..i].to_owned()));
                    tokens.push(Token::AttributeEnd);
                    mode_stack.pop();
                }
                _ => {
                    return Result::Err(TokenizeError::new(
                        mode_stack.pop(),
                        c,
                        uexpr[token_begin..i].to_owned(),
                        tokens,
                        "Illegal character detected in Mode::ScanningAttributeName.".to_owned(),
                    ))
                }
            },
            Mode::ScanningAttributeArguments => match c {
                SPACE | LINE_FEED => continue,
                ATTRIBUTE_END => {
                    tokens.push(Token::AttributeEnd);
                    mode_stack.pop();
                }
                STRING_START => {
                    mode_stack.push(Mode::ScanningValueString);
                    token_begin = i + 1;
                }
                ZERO...NINE => {
                    mode_stack.push(Mode::ScanningNumber);
                    token_begin = i;
                }
                MINUS => {
                    mode_stack.push(Mode::ScanningNegativeNumber);
                    token_begin = i;
                }
                DECIMAL_POINT => {
                    mode_stack.push(Mode::ScanningFloat);
                    token_begin = i;
                }
                BOOLEAN_FALSE | BOOLEAN_TRUE => {
                    mode_stack.push(Mode::ScanningValueBool);
                    token_begin = i;
                }
                _ => {
                    return Result::Err(TokenizeError::new(
                        mode_stack.pop(),
                        c,
                        uexpr[token_begin..i].to_owned(),
                        tokens,
                        "Scanned illegal character.".to_owned(),
                    ))
                }
            },
            Mode::ScanningValueString => match c {
                STRING_END => {
                    tokens.push(Token::ValueString(uexpr[token_begin..i].to_owned()));
                    mode_stack.pop();
                }
                _ => continue,
            },
            Mode::ScanningNumber => match c {
                ZERO...NINE => continue,
                DECIMAL_POINT => mode_stack.replace(Mode::ScanningFloat),
                SPACE | LINE_FEED => {
                    tokens.push(Token::ValueUInt(uexpr[token_begin..i].parse().unwrap()));
                    mode_stack.pop();
                }
                ELEMENT_END => {
                    tokens.push(Token::ValueUInt(uexpr[token_begin..i].parse().unwrap()));
                    tokens.push(Token::ElementEnd);
                    mode_stack.pop();
                    mode_stack.pop();
                }
                ATTRIBUTE_END => {
                    tokens.push(Token::ValueUInt(uexpr[token_begin..i].parse().unwrap()));
                    tokens.push(Token::AttributeEnd);
                    mode_stack.pop();
                    mode_stack.pop();
                }
                STRING_START => {
                    tokens.push(Token::ValueUInt(uexpr[token_begin..i].parse().unwrap()));
                    mode_stack.pop();

                    mode_stack.push(Mode::ScanningValueString);
                    token_begin = i + 1;
                }
                ELEMENT_START => {
                    tokens.push(Token::ValueUInt(uexpr[token_begin..i].parse().unwrap()));
                    mode_stack.pop();

                    mode_stack.push(Mode::ScanningElementName);
                    tokens.push(Token::ElementStart);
                    token_begin = i + 1;
                }
                _ => {
                    return Result::Err(TokenizeError::new(
                        mode_stack.pop(),
                        c,
                        uexpr[token_begin..i].to_owned(),
                        tokens,
                        "Illegal character detected in Mode::ScanningNumber.".to_owned(),
                    ))
                }
            },
            Mode::ScanningNegativeNumber => match c {
                ZERO...NINE => continue,
                DECIMAL_POINT => mode_stack.replace(Mode::ScanningFloat),
                SPACE | LINE_FEED => {
                    tokens.push(Token::ValueInt(uexpr[token_begin..i].parse().unwrap()));
                    mode_stack.pop();
                }
                ELEMENT_END => {
                    tokens.push(Token::ValueInt(uexpr[token_begin..i].parse().unwrap()));
                    tokens.push(Token::ElementEnd);
                    mode_stack.pop();
                    mode_stack.pop();
                }
                ATTRIBUTE_END => {
                    tokens.push(Token::ValueInt(uexpr[token_begin..i].parse().unwrap()));
                    tokens.push(Token::AttributeEnd);
                    mode_stack.pop();
                    mode_stack.pop();
                }
                STRING_START => {
                    tokens.push(Token::ValueInt(uexpr[token_begin..i].parse().unwrap()));
                    mode_stack.pop();

                    mode_stack.push(Mode::ScanningValueString);
                    token_begin = i + 1;
                }
                ELEMENT_START => {
                    tokens.push(Token::ValueInt(uexpr[token_begin..i].parse().unwrap()));
                    mode_stack.pop();

                    mode_stack.push(Mode::ScanningElementName);
                    tokens.push(Token::ElementStart);
                    token_begin = i + 1;
                }
                _ => {
                    return Result::Err(TokenizeError::new(
                        mode_stack.pop(),
                        c,
                        uexpr[token_begin..i].to_owned(),
                        tokens,
                        "Illegal character in Mode::ScanningNegativeNumber.".to_owned(),
                    ))
                }
            },
            Mode::ScanningFloat => match c {
                ZERO...NINE => continue,
                SPACE | LINE_FEED => {
                    tokens.push(Token::ValueFloat(uexpr[token_begin..i].parse().unwrap()));
                    mode_stack.pop();
                }
                ELEMENT_END => {
                    tokens.push(Token::ValueFloat(uexpr[token_begin..i].parse().unwrap()));
                    tokens.push(Token::ElementEnd);
                    mode_stack.pop();
                    mode_stack.pop();
                }
                ATTRIBUTE_END => {
                    tokens.push(Token::ValueFloat(uexpr[token_begin..i].parse().unwrap()));
                    tokens.push(Token::AttributeEnd);
                    mode_stack.pop();
                    mode_stack.pop();
                }
                STRING_START => {
                    tokens.push(Token::ValueFloat(uexpr[token_begin..i].parse().unwrap()));
                    mode_stack.pop();

                    mode_stack.push(Mode::ScanningValueString);
                    token_begin = i + 1;
                }
                ELEMENT_START => {
                    tokens.push(Token::ValueFloat(uexpr[token_begin..i].parse().unwrap()));
                    mode_stack.pop();

                    mode_stack.push(Mode::ScanningElementName);
                    tokens.push(Token::ElementStart);
                    token_begin = i + 1;
                }
                _ => {
                    return Result::Err(TokenizeError::new(
                        mode_stack.pop(),
                        c,
                        uexpr[token_begin..i].to_owned(),
                        tokens,
                        "Illegal character in Mode::ScanningFloat.".to_owned(),
                    ))
                }
            },
            Mode::ScanningValueBool => match c {
                SPACE | LINE_FEED => match &uexpr[token_begin..i] {
                    "f" => {
                        tokens.push(Token::ValueBool(false));
                        mode_stack.pop();
                    }
                    "t" => {
                        tokens.push(Token::ValueBool(true));
                        mode_stack.pop();
                    }
                    _ => {
                        return Result::Err(TokenizeError::new(
                            mode_stack.pop(),
                            c,
                            uexpr[token_begin..i].to_owned(),
                            tokens,
                            "Illegal spelling of f or t?".to_owned(),
                        ))
                    }
                },
                ELEMENT_END => match &uexpr[token_begin..i] {
                    "f" => {
                        tokens.push(Token::ValueBool(false));
                        tokens.push(Token::ElementEnd);
                        mode_stack.pop();
                        mode_stack.pop();
                    }
                    "t" => {
                        tokens.push(Token::ValueBool(true));
                        tokens.push(Token::ElementEnd);
                        mode_stack.pop();
                        mode_stack.pop();
                    }
                    _ => {
                        return Result::Err(TokenizeError::new(
                            mode_stack.pop(),
                            c,
                            uexpr[token_begin..i].to_owned(),
                            tokens,
                            "Illegal spelling of f or t?".to_owned(),
                        ))
                    }
                },
                ATTRIBUTE_END => match &uexpr[token_begin..i] {
                    "f" => {
                        tokens.push(Token::ValueBool(false));
                        tokens.push(Token::AttributeEnd);
                        mode_stack.pop();
                        mode_stack.pop();
                    }
                    "t" => {
                        tokens.push(Token::ValueBool(true));
                        tokens.push(Token::AttributeEnd);
                        mode_stack.pop();
                        mode_stack.pop();
                    }
                    _ => {
                        return Result::Err(TokenizeError::new(
                            mode_stack.pop(),
                            c,
                            uexpr[token_begin..i].to_owned(),
                            tokens,
                            "Illegal spelling of f or t?".to_owned(),
                        ))
                    }
                },
                STRING_START => match &uexpr[token_begin..i] {
                    "f" => {
                        tokens.push(Token::ValueBool(false));
                        mode_stack.pop();

                        mode_stack.push(Mode::ScanningValueString);
                        token_begin = i + 1;
                    }
                    "t" => {
                        tokens.push(Token::ValueBool(true));
                        mode_stack.pop();

                        mode_stack.push(Mode::ScanningValueString);
                        token_begin = i + 1;
                    }
                    _ => {
                        return Result::Err(TokenizeError::new(
                            mode_stack.pop(),
                            c,
                            uexpr[token_begin..i].to_owned(),
                            tokens,
                            "Illegal spelling of f or t?".to_owned(),
                        ))
                    }
                },
                ELEMENT_START => match &uexpr[token_begin..i] {
                    "f" => {
                        tokens.push(Token::ValueBool(false));
                        mode_stack.pop();

                        mode_stack.push(Mode::ScanningElementName);
                        tokens.push(Token::ElementStart);
                        token_begin = i + 1;
                    }
                    "t" => {
                        tokens.push(Token::ValueBool(true));
                        mode_stack.pop();

                        mode_stack.push(Mode::ScanningElementName);
                        tokens.push(Token::ElementStart);
                        token_begin = i + 1;
                    }
                    _ => {
                        return Result::Err(TokenizeError::new(
                            mode_stack.pop(),
                            c,
                            uexpr[token_begin..i].to_owned(),
                            tokens,
                            "Illegal spelling of f or t?".to_owned(),
                        ))
                    }
                },
                _ => {
                    return Result::Err(TokenizeError::new(
                        mode_stack.pop(),
                        c,
                        uexpr[token_begin..i].to_owned(),
                        tokens,
                        "Illegal character in Mode::ScanningValueBool.".to_owned(),
                    ))
                }
            },
        }
    }

    // Finished scanning let's inspect the state of the MODE_STACK and
    // add any final tokens and report and errors.
    match mode_stack.peek() {
        Mode::Start => {
            // Looks good this is as it should be
            // Check we are at bottom of the stack
        }
        Mode::ScanningValueBool => {
            // Perhaps a trailing boolean still needs to be put on the tokens vector
            match &uexpr[token_begin..] {
                "f" => {
                    tokens.push(Token::ValueBool(false));
                    mode_stack.pop();
                    // TODO check stack is at Mode::Start
                }
                "t" => {
                    tokens.push(Token::ValueBool(true));
                    mode_stack.pop();
                    // TODO check stack is at Mode::Start
                }
                _ => {
                    return Result::Err(TokenizeError::new(
                        mode_stack.pop(),
                        'E',
                        uexpr[token_begin..].to_owned(),
                        tokens,
                        "Illegal spelling of f or t? AT END OF SCAN".to_owned(),
                    ))
                }
            }
        }
        Mode::ScanningValueString => {}
        Mode::ScanningNumber => {
            tokens.push(Token::ValueUInt(uexpr[token_begin..].parse().unwrap()));
            mode_stack.pop();
            // TODO check statck is at Mode::Start
        }
        Mode::ScanningNegativeNumber => {
            tokens.push(Token::ValueInt(uexpr[token_begin..].parse().unwrap()));
            mode_stack.pop();
            // TODO check stack is at Mode::STARt
        }
        Mode::ScanningFloat => {
            tokens.push(Token::ValueFloat(uexpr[token_begin..].parse().unwrap()));
            mode_stack.pop();
            // TODO check stack
        }
        Mode::ScanningElementName => {}
        Mode::ScanningElementArguments => {
            return Result::Err(TokenizeError::new(
                mode_stack.pop(),
                'E',
                uexpr[token_begin..].to_owned(),
                tokens,
                "Scanned end of line character, inside element arguments.".to_owned(),
            ));
        }
        Mode::ScanningAttributeName => {}
        Mode::ScanningAttributeArguments => {
            return Result::Err(TokenizeError::new(
                mode_stack.pop(),
                'E',
                uexpr[token_begin..].to_owned(),
                tokens,
                "Scanned end of line character, inside attribute arguments.".to_owned(),
            ));
        }
    }

    Result::Ok(tokens)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn whitespace_uexpresions_produce_no_tokens() {
        assert!(tokenize("").unwrap().is_empty());
        assert!(tokenize(" ").unwrap().is_empty());
        assert!(tokenize("          ").unwrap().is_empty());
    }

    #[test]
    fn boolean_leaf_uexpressions_produce_valuebool_tokens() {
        assert_eq!(
            tokenize("f").unwrap().pop().unwrap(),
            Token::ValueBool(false)
        );
        assert_eq!(
            tokenize("t").unwrap().pop().unwrap(),
            Token::ValueBool(true)
        );
        assert_eq!(
            tokenize(" f ").unwrap().pop().unwrap(),
            Token::ValueBool(false)
        );
        assert_eq!(
            tokenize(" t ").unwrap().pop().unwrap(),
            Token::ValueBool(true)
        );
    }

    #[test]
    fn string_leaf_uexpressions_produce_valuestring_tokens() {
        assert_eq!(
            tokenize("\"hello world\"").unwrap().pop().unwrap(),
            Token::ValueString("hello world".to_owned())
        );
        assert_eq!(
            tokenize("   \"hello\"   ").unwrap().pop().unwrap(),
            Token::ValueString("hello".to_owned())
        );
    }

    #[test]
    fn uint_leaf_uexpressions_produce_valueuint_tokens() {
        assert_eq!(tokenize("1").unwrap().pop().unwrap(), Token::ValueUInt(1));
        assert_eq!(
            tokenize("   1").unwrap().pop().unwrap(),
            Token::ValueUInt(1)
        );
        assert_eq!(
            tokenize("   1 ").unwrap().pop().unwrap(),
            Token::ValueUInt(1)
        );
        assert_eq!(
            tokenize("   1    ").unwrap().pop().unwrap(),
            Token::ValueUInt(1)
        );
        assert_eq!(
            tokenize("1234567890").unwrap().pop().unwrap(),
            Token::ValueUInt(1234567890)
        );
        assert_eq!(
            tokenize("   0123456789   ").unwrap().pop().unwrap(),
            Token::ValueUInt(123456789)
        );
    }

    #[test]
    fn int_leaf_uexpressions_produce_valueint_tokens() {
        assert_eq!(
            tokenize("-1234567890").unwrap().pop().unwrap(),
            Token::ValueInt(-1234567890)
        );
        assert_eq!(
            tokenize("   -0123456789   ").unwrap().pop().unwrap(),
            Token::ValueInt(-123456789)
        );
    }

    #[test]
    fn float_leaf_uexpressions_produce_valuefloat_tokens() {
        assert_eq!(
            tokenize(".1234567890").unwrap().pop().unwrap(),
            Token::ValueFloat(0.123456789)
        );
        assert_eq!(
            tokenize("   .0123456789   ").unwrap().pop().unwrap(),
            Token::ValueFloat(0.0123456789)
        );
        assert_eq!(
            tokenize("0.1234567890").unwrap().pop().unwrap(),
            Token::ValueFloat(0.123456789)
        );
        assert_eq!(
            tokenize("   1.0123456789   ").unwrap().pop().unwrap(),
            Token::ValueFloat(1.0123456789)
        );
        assert_eq!(
            tokenize("-0.1234567890").unwrap().pop().unwrap(),
            Token::ValueFloat(-0.123456789)
        );
        assert_eq!(
            tokenize("   -1.0123456789   ").unwrap().pop().unwrap(),
            Token::ValueFloat(-1.0123456789)
        );
        assert_eq!(
            tokenize("-.1234567890").unwrap().pop().unwrap(),
            Token::ValueFloat(-0.123456789)
        );
        assert_eq!(
            tokenize("   -.0123456789   ").unwrap().pop().unwrap(),
            Token::ValueFloat(-0.0123456789)
        );
    }

    #[test]
    fn element_leaf_uexpressions_produce_element_tokens() {
        assert_eq!(
            tokenize("[myelementname]").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ElementEnd
            ]
        );

        assert_eq!(
            tokenize("   [myelementname]   ").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ElementEnd
            ]
        );
    }

    #[test]
    fn prefixed_element_leaf_uexpressions_produce_prefixed_element_tokens() {
        assert_eq!(
            tokenize("[mypfx:myelementname]").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementPrefix("mypfx".to_owned()),
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ElementEnd
            ]
        );

        assert_eq!(
            tokenize("        [mypfx:myelementname]      ").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementPrefix("mypfx".to_owned()),
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ElementEnd
            ]
        );

        assert_eq!(
            tokenize("[mypfx:myelementname    ]").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementPrefix("mypfx".to_owned()),
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ElementEnd
            ]
        );
    }

    #[test]
    fn element_parent_uexpressions_produce_element_and_leaf_tokens() {
        assert_eq!(
            tokenize("[myelementname t]").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueBool(true),
                Token::ElementEnd
            ]
        );
        assert_eq!(
            tokenize("  [myelementname t]  ").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueBool(true),
                Token::ElementEnd
            ]
        );
        assert_eq!(
            tokenize("[myelementname t  ]").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueBool(true),
                Token::ElementEnd,
            ]
        );
        assert_eq!(
            tokenize("[myelementname f]").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueBool(false),
                Token::ElementEnd
            ]
        );
        assert_eq!(
            tokenize("  [myelementname f]  ").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueBool(false),
                Token::ElementEnd
            ]
        );
        assert_eq!(
            tokenize("[myelementname f  ]").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueBool(false),
                Token::ElementEnd,
            ]
        );

        assert_eq!(
            tokenize("[myelementname 1]").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueUInt(1),
                Token::ElementEnd
            ]
        );
        assert_eq!(
            tokenize("  [myelementname 21]  ").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueUInt(21),
                Token::ElementEnd
            ]
        );
        assert_eq!(
            tokenize("[myelementname 3  ]").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueUInt(3),
                Token::ElementEnd,
            ]
        );
        assert_eq!(
            tokenize("[myelementname -1]").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueInt(-1),
                Token::ElementEnd
            ]
        );
        assert_eq!(
            tokenize("  [myelementname -21]  ").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueInt(-21),
                Token::ElementEnd
            ]
        );
        assert_eq!(
            tokenize("[myelementname -3  ]").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueInt(-3),
                Token::ElementEnd,
            ]
        );

        assert_eq!(
            tokenize("[myelementname 1.0]").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueFloat(1.0),
                Token::ElementEnd
            ]
        );
        assert_eq!(
            tokenize("[myelementname 1.]").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueFloat(1.0),
                Token::ElementEnd
            ]
        );
        assert_eq!(
            tokenize("  [myelementname 2.1]  ").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueFloat(2.1),
                Token::ElementEnd
            ]
        );
        assert_eq!(
            tokenize("[myelementname -3.32]").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueFloat(-3.32),
                Token::ElementEnd,
            ]
        );
        assert_eq!(
            tokenize("[myelementname -3.32  ]").unwrap(),
            vec![
                Token::ElementStart,
                Token::ElementLocalName("myelementname".to_owned()),
                Token::ValueFloat(-3.32),
                Token::ElementEnd,
            ]
        );
    }
}
