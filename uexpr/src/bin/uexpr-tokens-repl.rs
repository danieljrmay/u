use std::io;
use std::io::Write;
use uexpr::tokenizer;

fn main() {
    println!("UExpression Tokens REPL\n");

    println!("Exit the REPL by entering 'exit'.\n");

    loop {
        // Put the inside of this loop into a library function for testing purposes?
        print!("> ");
        io::stdout()
            .flush()
            .ok()
            .expect("Error: Unable to flush stdout.");

        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(_n) => {
                if input == "exit\n" {
                    break;
                }

                match tokenizer::tokenize(&input) {
                    Ok(output) => println!("{:?}", output),
                    Err(tokenizing_error) => {
                        println!("Error tokenizing input: {:?}", tokenizing_error)
                    }
                }
            }
            Err(error) => println!("Error reading input: {}", error),
        }
    }
}
