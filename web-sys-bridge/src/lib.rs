use wasm_bindgen::prelude::*;
use wasm_bindgen::JsValue;
use web_sys::Document;
use web_sys::Element;

fn create_web_sys_element(
    document: Document,
    namespace_uri: &str,
    qualified_name: &str,
) -> Result<Element, JsValue> {
    document.create_element_ns(Some(namespace_uri), qualified_name)
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
