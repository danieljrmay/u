use super::dom::Attribute;
use super::namespaces::Namespace;

pub const NAMESPACE_PREFIX: &str = "xml";
pub const NAMESPACE: Namespace = Namespace {
    prefix: NAMESPACE_PREFIX,
    uri: "http://www.w3.org/XML/1998/namespace",
};

pub fn attribute_factory<'a>(local_name: String, value: String) -> Attribute<'a> {
    match local_name.as_ref() {
        "lang" => Attribute::new(&NAMESPACE, local_name, value),
        "space" => Attribute::new(&NAMESPACE, local_name, value),
        "base" => Attribute::new(&NAMESPACE, local_name, value),
        "id" => Attribute::new(&NAMESPACE, local_name, value),
        "Father" => Attribute::new(&NAMESPACE, local_name, "Jon Bosak".to_string()),
        _ => panic!("Illegal attribute in the XML namespace."),
    }
}
