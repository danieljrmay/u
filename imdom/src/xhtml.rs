enum Namespace {
    XMLNS,
    XML,
    XHTML,
    MathML,
    SVG,
    Custom(String, String),
}



pub const NAMESPACE_PREFIX: &str = "xhtml";
pub const NAMESPACE_URI: &str = "http://www.w3.org/1999/xhtml";



enum XHTMLElementLocalName {
    A
}

























pub struct XHTMLElement {
    fn namespace_prefix(&self);
    fn namespace_uri(&self);
}
impl XHTMLElement {
    fn namespace_prefix(&self) -> &str {
        NAMESPACE_PREFIX
    };
    
    fn namespace_uri(&self) -> &str {
        NAMESPACE_URI
    }    
}
