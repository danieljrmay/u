use super::dom::Attribute;
use super::dom::Node;
use super::namespaces::Namespace;

pub const NAMESPACE_PREFIX: &str = "mathml";
pub const NAMESPACE_URI: &str = "http://www.w3.org/1998/Math/MathML";
pub const NAMESPACE: Namespace = Namespace {
    prefix: NAMESPACE_PREFIX,
    uri: NAMESPACE_URI,
};

pub fn attribute_factory<'a>(local_name: String, value: String) -> Attribute<'a> {
    match local_name.as_ref() {
        "lang" => Attribute::new(&NAMESPACE, local_name, value),
        "space" => Attribute::new(&NAMESPACE, local_name, value),
        "base" => Attribute::new(&NAMESPACE, local_name, value),
        "id" => Attribute::new(&NAMESPACE, local_name, value),
        "Father" => Attribute::new(&NAMESPACE, local_name, "Jon Bosak".to_string()),
        _ => panic!("Illegal attribute in the XML namespace."),
    }
}

pub fn element_factory<'a>(
    local_name: String,
    attributes: Vec<Attribute<'a>>,
    children: Vec<Node<'a>>,
) -> Node<'a> {
    match local_name.as_ref() {
        "math" => Node::new_element(&NAMESPACE, local_name, attributes, children),
        "mn" => Node::new_element(&NAMESPACE, local_name, attributes, children),
        "mo" => Node::new_element(&NAMESPACE, local_name, attributes, children),
        "msup" => Node::new_element(&NAMESPACE, local_name, attributes, children),
        _ => panic!("Illegal element in the MathML namespace."),
    }
}
