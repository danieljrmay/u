use imdom::dom::Node;
use imdom::dom::Serializer;
use imdom::namespaces::NamespaceRegistry;
use imdom::uexpr_token_parser;
use std::io;
use std::io::Write;
use uexpr::tokenizer;

fn main() {
    println!("UExpression Immutable DOM REPL\n");
    println!("Exit the REPL by entering 'exit'.\n");

    let namespace_registry = NamespaceRegistry::new();
    let default_namespace = namespace_registry.get("xml").unwrap();
    let serializer = Serializer::new();
    loop {
        // Put the inside of this loop into a library function for testing purposes?
        print!("> ");
        io::stdout()
            .flush()
            .ok()
            .expect("Error: Unable to flush stdout.");

        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(_n) => {
                if input == "exit\n" {
                    break;
                }

                match tokenizer::tokenize(&input) {
                    Ok(tokens) => {
                        println!("Tokens={:?}", tokens);
                        let node: Node = uexpr_token_parser::parse_tokens(
                            &namespace_registry,
                            default_namespace,
                            &tokens[..],
                        );
                        println!("XML={}", serializer.serialize(default_namespace, node));
                    }
                    Err(tokenizing_error) => {
                        println!("Error tokenizing input: {:?}", tokenizing_error)
                    }
                }
            }
            Err(error) => println!("Error reading input: {}", error),
        }
    }
}
