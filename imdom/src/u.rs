use super::dom::Attribute;
use super::dom::Node;
use super::namespaces::Namespace;

pub const NAMESPACE_PREFIX: &str = "u";
pub const NAMESPACE_URI: &str = "http://danieljrmay.com/u";
pub const NAMESPACE: Namespace = Namespace {
    prefix: NAMESPACE_PREFIX,
    uri: NAMESPACE_URI,
};

pub fn attribute_factory<'a>(local_name: String, value: String) -> Attribute<'a> {
    match local_name.as_ref() {
        "todo" => Attribute::new(&NAMESPACE, local_name, value),
        _ => panic!("Illegal attribute in the U namespace."),
    }
}

pub fn element_factory<'a>(
    local_name: String,
    attributes: Vec<Attribute<'a>>,
    children: Vec<Node<'a>>,
) -> Node<'a> {
    match local_name.as_ref() {
        "boolean" => Node::new_element(&NAMESPACE, local_name, attributes, children),
        "integer" => Node::new_element(&NAMESPACE, local_name, attributes, children),
        "plus" => Node::new_element(&NAMESPACE, local_name, attributes, children),
        _ => panic!("Illegal element in the MathML namespace."),
    }
}
