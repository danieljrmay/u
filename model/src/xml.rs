use super::DataType;
use super::Id;

pub enum XMLId {
    Comment,
    Father,
    Namespace(NamespaceId),
    Text,
    XML,
}

pub trait Namespace: DataType {
    fn prefix(&self) -> &str;
    fn uri(&self) -> &str;
}
pub enum NamespaceId {
    XMLNS,
    XML,
}
impl Namespace for NamespaceId {
    fn prefix(&self) -> &str {
        match self {
            NamespaceId::XMLNS => "xmlns",
            NamespaceId::XML => "xml",
        }
    }
    
    fn uri(&self) -> &str {
        match self {
            NamespaceId::XMLNS => "http://www.w3.org/2000/xmlns/",
            NamespaceId::XML => "http://www.w3.org/XML/1998/namespace",
        }        
    }
}
impl DataType for NamespaceId {
    fn execute(&self) -> Box<DataType>;
    fn id(&self) -> Id;
    fn read(&self) -> Option<&str>;
    fn to(&self, id: Id) -> Option<Box<DataType>>;
    fn write(&self, value: Option<String>);
}

impl DataType for XMLNSNamespace {}

pub trait Attribute {
    fn local_name(&self) -> &str;
    fn prefix(&self) -> &str;
    fn value(&self) -> &str;
}

pub trait Element: Node {
    fn attributes(&self) -> Vec<Box<Attribute>>;
    fn has_attributes(&self) -> bool;
    fn local_name(&self) -> &str;
    fn namespace_uri(&self) -> &str;
    fn prefix(&self) -> &str;
}

pub trait Node: DataType {
    fn child_nodes(&self) -> Vec<Box<Node>>;
    fn has_child_nodes(&self) -> bool;
    fn node_type(&self) -> NodeType;
    fn node_value(&self) -> Option<&str>;
}

pub enum NodeType {
    // TODO Should this be a Datatype
    Attriubte,
    Comment,
    Element,
    Text,
}

pub struct Text {
    pub text: String,
}
impl Text {
    pub fn new(text: String) -> Text {
        Text { text }
    }
}
impl Node for Text {
    fn child_nodes(&self) -> Vec<Box<Node>> {
        Vec::new()
    }

    fn has_child_nodes(&self) -> bool {
        false
    }

    fn node_type(&self) -> NodeType {
        NodeType::Text
    }

    fn node_value(&self) -> Option<&str> {
        Some(&self.text)
    }
}
impl DataType for Text {
    fn contents(&self) -> Option<Vec<Box<DataType>>> {
        None
    }

    fn id(&self) -> Id {
        Id::XML(XMLId::Text)
    }

    fn name(&self) -> &str {
        "text"
    }

    fn to(&self, id: Id) -> Option<Box<DataType>> {
        None
    }

    fn execute(&self) -> Box<DataType> {
        Box::new(Text::new(self.text.to_string()))
    }
}

#[derive(Debug)]
pub struct XML {
    pub xml: String,
}
impl XML {
    pub fn from_element<T: Element>(element: T) -> XML {
        let mut xml = String::new();
        let mut attributes_xml = String::new();

        if element.has_child_nodes() {
            let mut children_xml = String::new();

            for child in element.child_nodes() {
                match child.node_type() {
                    NodeType::Text => children_xml.push_str(child.node_value().unwrap()),
                    _ => panic!("Unhandled Node Type."),
                }
            }

            xml.push_str(&format!(
                "<{} xmlns=\"{}\"{}>{}</{}>",
                element.local_name(),
                element.namespace_uri(),
                attributes_xml,
                children_xml,
                element.local_name()
            ));
        } else {
            xml.push_str(&format!(
                "<{} xmlns=\"{}\"{}/>",
                element.local_name(),
                element.namespace_uri(),
                attributes_xml
            ));
        }

        XML::new(xml)
    }

    pub fn new(xml: String) -> XML {
        XML { xml }
    }
}
impl DataType for XML {
    fn contents(&self) -> Option<Vec<Box<DataType>>> {
        None
    }

    fn id(&self) -> Id {
        Id::XML(XMLId::XML)
    }

    fn name(&self) -> &str {
        "serialized_xml"
    }

    fn to(&self, id: Id) -> Option<Box<DataType>> {
        None
    }

    fn execute(&self) -> Box<DataType> {
        Box::new(XML::new(self.xml.to_string()))
    }
}
