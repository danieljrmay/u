use super::mathml::Cn;
use super::mathml::ContentElementId;
use super::mathml::MathMLId;
use super::mathml::Mn;
use super::mathml::PresentationElementId;
use super::DataType;
//use super::DataTypeClass;
use super::Id;
//use super::SerializedXML;
//use super::XMLId;
trait RustAddTrait {}

pub enum RustId {
    AddOperator,
    U32,
}

pub struct AddOperator {
    lhs: Box<RustAddTrait>,
    rhs: Box<RustAddTrait>,
}
impl AddOperator {
    fn new(lhs: Box<RustAddTrait>, rhs: Box<RustAddTrait>) -> AddOperator {
        AddOperator { lhs, rhs }
    }
}
impl DataType for AddOperator {
    fn contents(&self) -> Option<Vec<Box<DataType>>> {
        None
    }

    fn id(&self) -> Id {
        Id::Rust(RustId::AddOperator)
    }

    fn name(&self) -> &str {
        "+"
    }

    fn to(&self, id: Id) -> Option<Box<DataType>> {
        None
    }

    fn execute(&self) -> Box<DataType> {
        Box::new(U32::new(42))
    }
}

#[derive(Debug)]
pub struct U32 {
    pub value: u32,
}
impl U32 {
    fn new(value: u32) -> U32 {
        U32 { value }
    }
}
impl RustAddTrait {}
impl DataType for U32 {
    fn contents(&self) -> Option<Vec<Box<DataType>>> {
        None
    }

    fn id(&self) -> Id {
        Id::Rust(RustId::U32)
    }

    fn name(&self) -> &str {
        "u32"
    }

    fn to(&self, id: Id) -> Option<Box<DataType>> {
        match id {
            Id::MathML(MathMLId::ContentElement(ContentElementId::Cn)) => {
                Some(Box::new(Cn::from(self)))
            }
            Id::MathML(MathMLId::PresentationElement(PresentationElementId::Mn)) => {
                Some(Box::new(Mn::from(self)))
            }
            _ => None,
        }
    }

    fn execute(&self) -> Box<DataType> {
        Box::new(U32::new(self.value))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn rust_u8() {
        let my_u32 = U32::new(12);
        assert_eq!(format!("my_u32={:?}", &my_u32), "my_u32=U32 { value: 12 }");
    }
}
