use super::DataType;
use super::Id;

pub enum UId {
    UExpr,
}

pub struct UExpr {
    uexpr: String,
}
impl UExpr {
    /*
        pub fn from(boxed_data_type: Box<DataType>) -> UExpr {
            let mut uexpr = String::new();

            match boxed_data_type.contents() {
                Some(contents) => {
                    uexpr.push_str(&format!(
                        "[{}:{} {}]",
                        boxed_data_type.id().namespace_prefix(),
                        boxed_data_type.name(),
                        "CHILDREN"
                    ));
                }
                None => {
                    uexpr.push_str(&format!(
                        "[{}:{}]",
                        boxed_data_type.id().namespace_prefix(),
                        boxed_data_type.name()
                    ));
                }
            }

            UExpr::new(uexpr)
        }
    */
    pub fn new(uexpr: String) -> UExpr {
        UExpr { uexpr }
    }
}
impl DataType for UExpr {
    fn contents(&self) -> Option<Vec<Box<DataType>>> {
        None
    }

    fn id(&self) -> Id {
        Id::U(UId::UExpr)
    }

    fn name(&self) -> &str {
        "uexpr"
    }

    fn to(&self, id: Id) -> Option<Box<DataType>> {
        None
    }

    fn execute(&self) -> Box<DataType> {
        Box::new(UExpr::new(self.uexpr.to_string()))
    }
}
