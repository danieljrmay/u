use super::rust::U32;
use super::xml::Attribute;
use super::xml::Element;
use super::xml::Node;
use super::xml::NodeType;
use super::xml::Text;
use super::xml::XML;
use super::DataType;
use super::Id;

const MATHML_NAMESPACE_PREFIX: &str = "mathml";
const MATHML_NAMESPACE_URI: &str = "http://www.w3.org/1998/Math/MathML";

trait MathMLPresentationElement: Element {}

pub enum MathMLId {
    ContentElement(ContentElementId),
    PresentationElement(PresentationElementId),
    SerializedMathML,
}

pub enum ContentElementId {
    Cn,
}

pub enum PresentationElementId {
    Mn,
}

#[derive(Debug)]
pub struct Cn {
    value: String,
}
impl Cn {
    pub fn new(value: String) -> Cn {
        Cn { value }
    }
    pub fn from(U32: &U32) -> Cn {
        Cn::new(U32.value.to_string())
    }
}
impl DataType for Cn {
    fn contents(&self) -> Option<Vec<Box<DataType>>> {
        None
    }

    fn id(&self) -> Id {
        Id::MathML(MathMLId::ContentElement(ContentElementId::Cn))
    }

    fn name(&self) -> &str {
        "cn"
    }

    fn to(&self, id: Id) -> Option<Box<DataType>> {
        match id {
            _ => None,
        }
    }

    fn execute(&self) -> Box<DataType> {
        Box::new(Cn::new(self.value.to_string()))
    }
}

#[derive(Debug)]
pub struct Mn {
    value: String,
}
impl Mn {
    pub fn from(U32: &U32) -> Mn {
        Mn::new(U32.value.to_string())
    }
    pub fn new(value: String) -> Mn {
        Mn { value }
    }
}
impl DataType for Mn {
    fn contents(&self) -> Option<Vec<Box<DataType>>> {
        None
    }

    fn id(&self) -> Id {
        Id::MathML(MathMLId::PresentationElement(PresentationElementId::Mn))
    }

    fn name(&self) -> &str {
        "mn"
    }

    fn to(&self, id: Id) -> Option<Box<DataType>> {
        None
    }

    fn execute(&self) -> Box<DataType> {
        Box::new(Mn::new(self.value.to_string()))
    }
}
impl MathMLPresentationElement for Mn {}
impl Element for Mn {
    fn attributes(&self) -> Vec<Box<Attribute>> {
        Vec::new()
    }

    fn has_attributes(&self) -> bool {
        false
    }

    fn local_name(&self) -> &str {
        self.name()
    }

    fn namespace_uri(&self) -> &str {
        MATHML_NAMESPACE_URI
    }

    fn prefix(&self) -> &str {
        MATHML_NAMESPACE_PREFIX
    }
}
impl Node for Mn {
    fn child_nodes(&self) -> Vec<Box<Node>> {
        vec![Box::new(Text::new(self.value.to_string()))]
    }

    fn has_child_nodes(&self) -> bool {
        true
    }

    fn node_type(&self) -> NodeType {
        NodeType::Element
    }

    fn node_value(&self) -> Option<&str> {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn mathml_mn() {
        let my_mn = Mn::new("18".to_string());
        assert_eq!(format!("my_mn={:?}", &my_mn), "my_mn=Mn { value: \"18\" }");

        assert_eq!(XML::from_element(my_mn).xml, "HELOO");
    }
}
