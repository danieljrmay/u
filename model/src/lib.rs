//pub mod mathml;
//pub mod rust;
//pub mod u;
pub mod xml;

pub trait Cast<T> {
    fn cast(&self) -> T
}
pub trait Execute<T> {
    fn execute(&self) -> T;
}

pub trait DataType: Clone {
    fn execute(&self) -> Box<DataType>;
    fn id(&self) -> Id;
    fn to(&self, id: Id) -> Option<Box<DataType>>;
}

pub enum Id {
    //    MathML(mathml::MathMLId),
    //    Rust(rust::RustId),
    //    U(u::UId),
    XML(xml::XMLId),
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
