use super::dom::Attribute;
use super::dom::AttributeSet;
use super::dom::Element;
use super::dom::Namespace;
use super::dom::Node;
use super::dom::NodeList;

pub const XHTML_NAMESPACE: Namespace = Namespace {
    prefix: "xhtml",
    uri: "http://www.w3.org/1999/xhtml",
};

pub enum XHTMLAttribute {
    Title(String),
}
impl Attribute for XHTMLAttribute {
    fn local_name(&self) -> &str {
        match self {
            XHTMLAttribute::Title(_title) => "title",
        }
    }

    fn namespace(&self) -> &Namespace {
        &XHTML_NAMESPACE
    }

    fn value(&self) -> &str {
        match self {
            XHTMLAttribute::Title(title) => &title,
        }
    }
}

// TODO: make factory method part of Attribute trait?
pub fn xhtml_attribute_factory(local_name: &str, value: String) -> XHTMLAttribute {
    match local_name {
        "title" => XHTMLAttribute::Title(value),
        _ => panic!("Unkown XHTML attribute"),
    }
}

pub enum XHTMLElement {
    Paragraph(AttributeSet, NodeList),
}
impl Element for XHTMLElement {
    fn attributes(&self) -> Option<&AttributeSet> {
        match self {
            XHTMLElement::Paragraph(attribute_set, _node_list) => Some(attribute_set),
        }
    }

    fn children(&self) -> Option<&NodeList> {
        match self {
            XHTMLElement::Paragraph(_attribute_set, node_list) => Some(node_list),
        }
    }
    fn local_name(&self) -> &str {
        match self {
            XHTMLElement::Paragraph(_attribute_set, _node_list) => "p",
        }
    }

    fn namespace(&self) -> &Namespace {
        &XHTML_NAMESPACE
    }
}

fn xhtml_element_factory(
    local_name: &str,
    attributes: AttributeSet,
    children: NodeList,
) -> XHTMLElement {
    match local_name {
        "p" => XHTMLElement::Paragraph(attributes, children),
        _ => panic!("Unknown XHTML element"),
    }
}

pub fn xhtml_element_node_factory(
    local_name: &str,
    attributes: AttributeSet,
    children: NodeList,
) -> Node {
    Node::Element(Box::new(xhtml_element_factory(
        local_name, attributes, children,
    )))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn xhtml_namespace() {
        assert_eq!(XHTML_NAMESPACE.prefix, "xhtml");
        assert_eq!(XHTML_NAMESPACE.uri, "http://www.w3.org/1999/xhtml");
    }

    #[test]
    fn xhtml_attribute() {
        let title = XHTMLAttribute::Title("My tooltip.".to_string());
        assert_eq!(title.local_name(), "title");
        assert_eq!(title.namespace(), &XHTML_NAMESPACE);
        assert_eq!(title.value(), "My tooltip.");
    }

    #[test]
    fn xhtml_attribute_factory_tests() {
        let title = xhtml_attribute_factory("title", String::from("My factory tooltip."));
        assert_eq!(title.local_name(), "title");
        assert_eq!(title.namespace(), &XHTML_NAMESPACE);
        assert_eq!(title.value(), "My factory tooltip.");
    }

    #[test]
    fn xhtml_element() {
        let attributes = AttributeSet::new();
        let children = NodeList::new();
        let p = XHTMLElement::Paragraph(attributes, children);

        assert_eq!(p.local_name(), "p");
        assert_eq!(p.namespace(), &XHTML_NAMESPACE);
    }

    #[test]
    fn xhtml_element_factory_tests() {
        let attributes = AttributeSet::new();
        let children = NodeList::new();
        let p = xhtml_element_factory("p", attributes, children);
        assert_eq!(p.local_name(), "p");
        assert_eq!(p.namespace(), &XHTML_NAMESPACE);
    }
}
