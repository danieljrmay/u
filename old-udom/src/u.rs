use super::dom::Attribute;
use super::dom::AttributeSet;
use super::dom::Element;
use super::dom::Namespace;
use super::dom::Node;
use super::dom::NodeList;

pub const U_NAMESPACE: Namespace = Namespace {
    prefix: "u",
    uri: "https://danieljrmay.com/U",
};

const FALSE: &str = "false";
const INTEGER: &str = "integer";
const NATURAL: &str = "natural";
const PLUS: &str = "plus";
const STRING: &str = "string";
const TRUE: &str = "true";
pub enum UElement {
    False,
    Integer(isize),
    Natural(usize),
    Plus(NodeList),
    String(String),
    True,
}
impl Element for UElement {
    fn attributes(&self) -> Option<&AttributeSet> {
        None
    }

    fn children(&self) -> Option<&NodeList> {
        match self {
            UElement::Plus(node_list) => Some(node_list),
            _ => None,
        }
    }

    fn local_name(&self) -> &str {
        match self {
            UElement::False => FALSE,
            UElement::Integer(_integer) => INTEGER,
            UElement::Natural(_natural) => NATURAL,
            UElement::Plus(_node_list) => PLUS,
            UElement::String(_string) => STRING,
            UElement::True => TRUE,
        }
    }

    fn namespace(&self) -> &Namespace {
        &U_NAMESPACE
    }
}
impl UElement {
    fn evaluate(&self) -> UElement {
        match self {
            UElement::False => UElement::False,
            UElement::Integer(integer) => UElement::Integer(*integer),
            UElement::Natural(natural) => UElement::Natural(*natural),
            UElement::String(string) => UElement::String(string.to_string()),
            UElement::True => UElement::True,
            UElement::Plus(node_list) => {
                let mut usize_sum: usize;

                for node in &node_list.nodes {
                    match node {
                        Node::Element(element) => match **element {
                            UElement::Natural(natural) => usize_sum += natural,
                            _ => panic!("Non Natual child of Plus."),
                        },
                        _ => panic!("Plus should only have Node::Element children."),
                    }
                }

                usize_sum = 17;
                UElement::Natural(usize_sum)
            }
        }
    }
}

fn u_element_factory(local_name: &str, attributes: AttributeSet, children: NodeList) -> UElement {
    match local_name {
        FALSE => UElement::False,
        INTEGER => UElement::Integer(-1),
        NATURAL => UElement::Natural(2),
        PLUS => UElement::Plus(children),
        STRING => UElement::String("my string".to_string()),
        TRUE => UElement::True,

        _ => panic!("Unknown U element"),
    }
}

pub fn u_element_node_factory(
    local_name: &str,
    attributes: AttributeSet,
    children: NodeList,
) -> Node {
    Node::Element(Box::new(u_element_factory(
        local_name, attributes, children,
    )))
}
