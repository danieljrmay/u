use super::dom::Attribute;
use super::dom::AttributeSet;
use super::dom::AttributeValue;
use super::dom::Namespace;
use super::dom::NamespaceSet;
use super::dom::Node;
use super::dom::NodeList;
use super::dom::XMLNS_NAMESPACE;

pub const XML_NAMESPACE: Namespace = Namespace {
    prefix: "xml",
    uri: "http://www.w3.org/XML/1998/namespace",
};

pub enum XMLAttribute {
    Base(URI),
    Father,
    Id(Id),
    Lang(LangCode),
    Space(XMLSpace),
}
impl Attribute for XMLAttribute {
    fn local_name(&self) -> &str {
        match self {
            XMLAttribute::Base(_uri) => "base",
            XMLAttribute::Father => "Father",
            XMLAttribute::Id(_id) => "id",
            XMLAttribute::Lang(_lang_code) => "lang",
            XMLAttribute::Space(_xml_space) => "space",
        }
    }

    fn namespace(&self) -> &Namespace {
        &XML_NAMESPACE
    }

    fn value(&self) -> &str {
        match self {
            XMLAttribute::Base(uri) => uri.value(),
            XMLAttribute::Father => "Jon Bosak",
            XMLAttribute::Id(id) => id.value(),
            XMLAttribute::Lang(lang_code) => lang_code.value(),
            XMLAttribute::Space(xml_space) => xml_space.value(),
        }
    }
}

pub struct Id {
    id: String,
}
impl AttributeValue for Id {
    fn value(&self) -> &str {
        &self.id
    }
}

pub enum XMLSpace {
    Default,
    Preserve,
}
impl AttributeValue for XMLSpace {
    fn value(&self) -> &str {
        match self {
            XMLSpace::Default => "default",
            XMLSpace::Preserve => "preserve",
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn xml_namespace() {
        assert_eq!(XML_NAMESPACE.prefix, "xml");
        assert_eq!(XML_NAMESPACE.uri, "http://www.w3.org/XML/1998/namespace");
    }

    #[test]
    fn xml_attributes() {
        assert_eq!(XMLAttribute::Father.namespace(), &XML_NAMESPACE);
        assert_eq!(XMLAttribute::Father.local_name(), "Father");
        assert_eq!(XMLAttribute::Father.value(), "Jon Bosak");
    }
}

pub struct XMLSerializer {}
impl XMLSerializer {
    pub fn new() -> XMLSerializer {
        XMLSerializer {}
    }

    pub fn serialize(&self, node: &Node, default_namespace: &Namespace) -> String {
        match node {
            Node::Text(text) => text.text.to_string(), // TODO implement to_string on Text?
            Node::Comment(comment) => format!("<!--{}-->", comment.comment), //TODO implement to_string on Comment?
            Node::Element(element) => {
                let default_namespace_xml =
                    self.serialize_default_namespace(element.namespace(), default_namespace);

                let mut prefixed_namespaces_and_attributes_xml: String;
                match element.attributes() {
                    Some(attribute_set) => {
                        prefixed_namespaces_and_attributes_xml =
                            self.serialize_attribute_set(attribute_set, element.namespace());
                    }
                    None => {
                        prefixed_namespaces_and_attributes_xml = String::new();
                    }
                }

                match element.children() {
                    Some(node_list) => {
                        let children_xml = self.serialize_node_list(node_list, element.namespace());
                        return format!(
                            "<{}{}{}>{}</{}>",
                            element.local_name(),
                            default_namespace_xml,
                            prefixed_namespaces_and_attributes_xml,
                            children_xml,
                            element.local_name()
                        );
                    }
                    None => {
                        return format!(
                            "<{}{}{}/>",
                            element.local_name(),
                            default_namespace_xml,
                            prefixed_namespaces_and_attributes_xml,
                        );
                    }
                }
            }
        }
    }

    fn serialize_default_namespace(
        &self,
        namespace: &Namespace,
        default_namespace: &Namespace,
    ) -> String {
        if namespace == default_namespace
            || namespace == &XML_NAMESPACE
            || namespace == &XMLNS_NAMESPACE
        {
            return String::new();
        }

        format!(" xmlns=\"{}\"", namespace.uri)
    }

    fn serialize_prefixed_namespace(
        &self,
        namespace: &Namespace,
        default_namespace: &Namespace,
    ) -> String {
        if namespace == default_namespace
            || namespace == &XML_NAMESPACE
            || namespace == &XMLNS_NAMESPACE
        // TODO or already defined in parent
        {
            return String::new();
        }

        format!(" xmlns:{}=\"{}\"", namespace.prefix, namespace.uri)
    }

    fn serialize_attribute(
        &self,
        boxed_attribute: &Box<Attribute>,
        default_namespace: &Namespace,
    ) -> String {
        if boxed_attribute.namespace() == default_namespace {
            return format!(
                " {}=\"{}\"",
                boxed_attribute.local_name(),
                boxed_attribute.value()
            );
        }

        format!(
            " {}:{}=\"{}\"",
            boxed_attribute.namespace().prefix,
            boxed_attribute.local_name(),
            boxed_attribute.value(),
        )
    }

    fn serialize_attribute_set(
        &self,
        attribute_set: &AttributeSet,
        default_namespace: &Namespace,
    ) -> String {
        let mut prefixed_namespaces_xml = String::new();
        let mut attributes_xml = String::new();

        for boxed_attribute in &attribute_set.set {
            //TODO do something for prefixed_namespaces_xml
            attributes_xml.push_str(&self.serialize_attribute(boxed_attribute, default_namespace));
        }

        format!("{}{}", prefixed_namespaces_xml, attributes_xml)
    }

    fn serialize_node_list(&self, node_list: &NodeList, default_namespace: &Namespace) -> String {
        let mut xml = String::new();

        for node in &node_list.nodes {
            xml.push_str(&self.serialize(&node, default_namespace));
        }

        xml
    }
}

#[cfg(test)]
mod xml_serializer_tests {
    use super::super::dom::Node;
    use super::*;

    #[test]
    fn serialize_comment() {
        let serializer = XMLSerializer::new();
        let comment_string = String::from("This is my comment string.");
        let comment = Node::new_comment(comment_string);

        assert_eq!(
            serializer.serialize(&comment, &XML_NAMESPACE),
            "<!--This is my comment string.-->"
        );
    }

    #[test]
    fn serialize_text() {
        let serializer = XMLSerializer::new();
        let text_string = String::from("This is my text string.");
        let text = Node::new_text(text_string);

        assert_eq!(
            serializer.serialize(&text, &XML_NAMESPACE),
            "This is my text string."
        );
    }

}

// Move to other modules?
pub struct URI {
    uri: String,
}
impl AttributeValue for URI {
    fn value(&self) -> &str {
        &self.uri
    }
}

pub enum LangCode {
    EnGB,
}
impl AttributeValue for LangCode {
    fn value(&self) -> &str {
        match self {
            LangCode::EnGB => "en-GB",
        }
    }
}
