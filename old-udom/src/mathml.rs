use super::dom::Attribute;
use super::dom::AttributeSet;
use super::dom::AttributeValue;
use super::dom::Element;
use super::dom::Namespace;
use super::dom::Node;
use super::dom::NodeList;

pub const MATHML_NAMESPACE: Namespace = Namespace {
    prefix: "mathml",
    uri: "http://www.w3.org/1998/Math/MathML",
};

const MATH_VARIANT: &str = "mathvariant";
const MATH_VARIANT_NORMAL: &str = "normal";
const MATH_VARIANT_BOLD: &str = "bold";
const MATH_VARIANT_ITALIC: &str = "italic";
const MATH_VARIANT_BOLD_ITALIC: &str = "bold-italic";
const MATH_VARIANT_DOUBLE_STRUCK: &str = "double-struck";
const MATH_VARIANT_BOLD_FRAKTUR: &str = "bold-fraktur";
const MATH_VARIANT_SCRIPT: &str = "script";
const MATH_VARIANT_BOLD_SCRIPT: &str = "bold-script";
const MATH_VARIANT_FRAKTUR: &str = "fraktur";
const MATH_VARIANT_SANS_SERIF: &str = "sans-serif";
const MATH_VARIANT_BOLD_SANS_SERIF: &str = "bold-sans-serif";
const MATH_VARIANT_SANS_SERIF_ITALIC: &str = "sans-serif-italic";
const MATH_VARIANT_SANS_SERIF_BOLD_ITALIC: &str = "sans-serif-bold-italic";
const MATH_VARIANT_MONOSPACE: &str = "monospace";
const MATH_VARIANT_INITIAL: &str = "initial";
const MATH_VARIANT_TAILED: &str = "tailed";
const MATH_VARIANT_LOOPED: &str = "looped";
const MATH_VARIANT_STRETCHED: &str = "stretched";

pub enum MathMLAttribute {
    MathVariant(MathVariantValue),
}
impl Attribute for MathMLAttribute {
    fn local_name(&self) -> &str {
        match self {
            MathMLAttribute::MathVariant(_math_variant_value) => MATH_VARIANT,
        }
    }

    fn namespace(&self) -> &Namespace {
        &MATHML_NAMESPACE
    }

    fn value(&self) -> &str {
        match self {
            MathMLAttribute::MathVariant(math_variant_value) => math_variant_value.value(),
        }
    }
}

pub enum MathVariantValue {
    Normal,
    Bold,
    Italic,
    BoldItalic,
    DoubleStruck,
    BoldFraktur,
    Script,
    BoldScript,
    Fraktur,
    SansSerif,
    BoldSansSerif,
    SansSerifItalic,
    SansSerifBoldItalic,
    Monospace,
    Initial,
    Tailed,
    Looped,
    Stretched,
}
impl AttributeValue for MathVariantValue {
    fn value(&self) -> &str {
        match self {
            MathVariantValue::Normal => MATH_VARIANT_NORMAL,
            MathVariantValue::Bold => MATH_VARIANT_BOLD,
            MathVariantValue::Italic => MATH_VARIANT_ITALIC,
            MathVariantValue::BoldItalic => MATH_VARIANT_BOLD_ITALIC,
            MathVariantValue::DoubleStruck => MATH_VARIANT_DOUBLE_STRUCK,
            MathVariantValue::BoldFraktur => MATH_VARIANT_BOLD_FRAKTUR,
            MathVariantValue::Script => MATH_VARIANT_SCRIPT,
            MathVariantValue::BoldScript => MATH_VARIANT_BOLD_SCRIPT,
            MathVariantValue::Fraktur => MATH_VARIANT_FRAKTUR,
            MathVariantValue::SansSerif => MATH_VARIANT_SANS_SERIF,
            MathVariantValue::BoldSansSerif => MATH_VARIANT_BOLD_SANS_SERIF,
            MathVariantValue::SansSerifItalic => MATH_VARIANT_SANS_SERIF_ITALIC,
            MathVariantValue::SansSerifBoldItalic => MATH_VARIANT_SANS_SERIF_BOLD_ITALIC,
            MathVariantValue::Monospace => MATH_VARIANT_MONOSPACE,
            MathVariantValue::Initial => MATH_VARIANT_INITIAL,
            MathVariantValue::Tailed => MATH_VARIANT_TAILED,
            MathVariantValue::Looped => MATH_VARIANT_LOOPED,
            MathVariantValue::Stretched => MATH_VARIANT_STRETCHED,
        }
    }
}
impl MathVariantValue {
    fn new(value: &str) -> MathVariantValue {
        match value {
            MATH_VARIANT_NORMAL => MathVariantValue::Normal,
            MATH_VARIANT_BOLD => MathVariantValue::Bold,
            MATH_VARIANT_ITALIC => MathVariantValue::Italic,
            MATH_VARIANT_BOLD_ITALIC => MathVariantValue::BoldItalic,
            MATH_VARIANT_DOUBLE_STRUCK => MathVariantValue::DoubleStruck,
            MATH_VARIANT_BOLD_FRAKTUR => MathVariantValue::BoldFraktur,
            MATH_VARIANT_SCRIPT => MathVariantValue::Script,
            MATH_VARIANT_BOLD_SCRIPT => MathVariantValue::BoldScript,
            MATH_VARIANT_FRAKTUR => MathVariantValue::Fraktur,
            MATH_VARIANT_SANS_SERIF => MathVariantValue::SansSerif,
            MATH_VARIANT_BOLD_SANS_SERIF => MathVariantValue::BoldSansSerif,
            MATH_VARIANT_SANS_SERIF_ITALIC => MathVariantValue::SansSerifItalic,
            MATH_VARIANT_SANS_SERIF_BOLD_ITALIC => MathVariantValue::SansSerifBoldItalic,
            MATH_VARIANT_MONOSPACE => MathVariantValue::Monospace,
            MATH_VARIANT_INITIAL => MathVariantValue::Initial,
            MATH_VARIANT_TAILED => MathVariantValue::Tailed,
            MATH_VARIANT_LOOPED => MathVariantValue::Looped,
            MATH_VARIANT_STRETCHED => MathVariantValue::Stretched,
            _ => panic!("Unkown MathVariantValue attribute value"),
        }
    }
}
// TODO: make factory method part of Attribute trait?
pub fn mathml_attribute_factory(local_name: &str, value: String) -> MathMLAttribute {
    match local_name {
        MATH_VARIANT => MathMLAttribute::MathVariant(MathVariantValue::new(&value)),
        _ => panic!("Unkown MathML attribute"),
    }
}

const MATH: &str = "math";
pub enum MathMLElement {
    Math(AttributeSet, NodeList),
}
impl Element for MathMLElement {
    fn attributes(&self) -> Option<&AttributeSet> {
        match self {
            MathMLElement::Math(attribute_set, _node_list) => Some(attribute_set),
        }
    }

    fn children(&self) -> Option<&NodeList> {
        match self {
            MathMLElement::Math(_attribute_set, node_list) => Some(node_list),
        }
    }
    fn local_name(&self) -> &str {
        match self {
            MathMLElement::Math(_attribute_set, _node_list) => MATH,
        }
    }

    fn namespace(&self) -> &Namespace {
        &MATHML_NAMESPACE
    }
}

fn mathml_element_factory(
    local_name: &str,
    attributes: AttributeSet,
    children: NodeList,
) -> MathMLElement {
    match local_name {
        MATH => MathMLElement::Math(attributes, children),
        _ => panic!("Unknown MathML element"),
    }
}

pub fn mathml_element_node_factory(
    local_name: &str,
    attributes: AttributeSet,
    children: NodeList,
) -> Node {
    Node::Element(Box::new(mathml_element_factory(
        local_name, attributes, children,
    )))
}
