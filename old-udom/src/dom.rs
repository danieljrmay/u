use super::xml::XMLSerializer;
use super::xml::XML_NAMESPACE;
use std::cmp::PartialEq;
use std::collections::BTreeMap;
use std::collections::HashSet;

pub const XMLNS_NAMESPACE: Namespace = Namespace {
    prefix: "xmlns",
    uri: "http://www.w3.org/2000/xmlns/",
};

#[derive(Debug)]
pub struct Namespace {
    pub prefix: &'static str,
    pub uri: &'static str,
}
impl PartialEq for Namespace {
    fn eq(&self, other: &Namespace) -> bool {
        self.uri == other.uri
    }
}
impl Namespace {
    pub fn new(prefix: &'static str, uri: &'static str) -> Namespace {
        // TODO validate prefix and uri
        Namespace { prefix, uri }
    }
}

pub struct NamespaceSet {
    set: HashSet<&'static str>,
}
impl NamespaceSet {
    pub fn new() -> NamespaceSet {
        let mut set = HashSet::new();
        set.insert(XMLNS_NAMESPACE.uri);
        set.insert(XML_NAMESPACE.uri);

        NamespaceSet { set }
    }

    pub fn contains(&self, namespace: &Namespace) -> bool {
        self.set.contains(namespace.uri)
    }

    pub fn insert(&mut self, namespace: &Namespace) {
        self.set.insert(namespace.uri);
    }
}

#[cfg(test)]
mod namespace_tests {
    use super::*;

    #[test]
    fn namespace() {
        let ns = Namespace::new("myprefix", "myuri");
        assert_eq!(ns.prefix, "myprefix");
        assert_eq!(ns.uri, "myuri");

        assert_eq!(XMLNS_NAMESPACE.prefix, "xmlns");
        assert_eq!(XMLNS_NAMESPACE.uri, "http://www.w3.org/2000/xmlns/");

        assert_ne!(XMLNS_NAMESPACE, XML_NAMESPACE);
        let ns_a = Namespace::new("a", "sameuri");
        let ns_b = Namespace::new("b", "sameuri");
        assert_eq!(ns_a, ns_b);
    }

    #[test]
    fn namespace_set() {
        let ns = Namespace::new("myprefix", "myuri");
        let mut nsset = NamespaceSet::new();
        assert!(nsset.contains(&XMLNS_NAMESPACE));
        assert!(nsset.contains(&XML_NAMESPACE));
        assert!(!nsset.contains(&ns));

        nsset.insert(&ns);
        assert!(nsset.contains(&ns));
    }
}

pub trait Attribute {
    fn local_name(&self) -> &str;
    fn namespace(&self) -> &Namespace;
    fn value(&self) -> &str;
}

pub struct AttributeSet {
    pub set: Vec<Box<Attribute>>,
}
impl AttributeSet {
    pub fn new() -> AttributeSet {
        AttributeSet { set: Vec::new() }
    }

    fn insert(&mut self, attribute: Box<Attribute>) {
        self.set.push(attribute);
    }

    fn is_empty(&self) -> bool {
        self.set.is_empty()
    }
}

pub trait AttributeValue {
    fn value(&self) -> &str;
}

#[cfg(test)]
mod attribute_tests {
    use super::super::xml::XMLAttribute;
    use super::super::xml::XMLSpace;
    use super::*;

    // TODO tidy up and add attribute tests

    #[test]
    fn attribute_set_test() {
        let mut attr_set = AttributeSet::new();
        assert!(attr_set.is_empty());
        attr_set.insert(Box::new(XMLAttribute::Space(XMLSpace::Preserve)));
        assert!(!attr_set.is_empty());
    }
}

pub struct Comment {
    pub comment: String,
}
impl Comment {
    fn new(comment: String) -> Comment {
        // TODO validate comment e.g. no "--" sequence, etc.
        Comment { comment }
    }
}

pub trait Element {
    fn attributes(&self) -> Option<&AttributeSet>;
    fn children(&self) -> Option<&NodeList>;
    fn local_name(&self) -> &str;
    fn namespace(&self) -> &Namespace;
}

pub enum Node {
    Comment(Comment),
    Element(Box<Element>),
    Text(Text),
}
impl Node {
    pub fn new_comment(comment: String) -> Node {
        // TODO validate comment e.g. no "--" sequence, etc.
        Node::Comment(Comment { comment })
    }

    pub fn new_text(text: String) -> Node {
        // TODO validate text
        Node::Text(Text { text })
    }
}

pub struct NodeList {
    pub nodes: Vec<Node>,
}
impl NodeList {
    pub fn new() -> NodeList {
        NodeList { nodes: Vec::new() }
    }

    fn append(&mut self, node: Node) {
        self.nodes.push(node)
    }

    fn is_empty(&self) -> bool {
        self.nodes.is_empty()
    }
}

#[cfg(test)]
mod node_tests {
    use super::*;

    #[test]
    fn node_list_test() {
        let mut nl = NodeList::new();
        assert!(nl.is_empty());
        nl.append(Node::Text(Text::new("My text node.".to_string())));
        assert!(!nl.is_empty());
    }
}

pub struct Text {
    pub text: String,
}
impl Text {
    fn new(text: String) -> Text {
        // TODO add validating code
        Text { text }
    }
}
